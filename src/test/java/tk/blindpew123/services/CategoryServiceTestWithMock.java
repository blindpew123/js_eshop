package tk.blindpew123.services;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import tk.blindpew123.services.dao.CategoryDao;
import tk.blindpew123.services.dto.CategoryDto;
 import tk.blindpew123.services.entities.Category;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;



public class CategoryServiceTestWithMock {

    @Mock
    private CategoryDao categoryDao;

    @InjectMocks
    private CategoryService service;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllCategoriesAsDtoList_Success(){
        when(categoryDao.findAllCategoriesAsDtoList())
                .thenReturn(Arrays.asList(new CategoryDto()));

        assertEquals(1, service.getAllCategoriesAsDtoList().size());

    }

    @Test
    public void testSaveOrUpdate_SaveSuccess(){
        doNothing().when(categoryDao).renumberSameLevelCategories(null,1,1);
        when(categoryDao.saveOrUpdate(any(Category.class))).thenAnswer(category->{
            ((Category)category).setIdCategory(1000);
            return category;
        });
        CategoryDto cat = new CategoryDto();
        cat.setCategoryName("qqqq");
        cat.setCategoryNumber(1);
        Category catTest = service.saveOrUpdate(cat);
        assertEquals(1000,catTest.getIdCategory());
    }

}
