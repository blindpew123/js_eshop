package tk.blindpew123.services.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.services.CategoryService;
import tk.blindpew123.services.entities.Category;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class ShopItemDaoImplTest {

    @Autowired
    private ShopItemDao shopItemDao;

    @Autowired
    private CategoryService categoryService;

    public void setShopItemDao(ShopItemDao shopItemDao) {
        this.shopItemDao = shopItemDao;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Test
    public void testFindShopItemsWithTheirParamValues(){
        Category category = categoryService.getCategoryById(1L);
        List<Object[]> list = shopItemDao.findAllShopItemsWithParameterValuesForCategory(category);
        assertEquals(2,list.size());
    }


}
