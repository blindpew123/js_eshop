package tk.blindpew123.services.dao;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.services.CategoryService;
import tk.blindpew123.services.dto.ParameterDto;
import tk.blindpew123.services.entities.*;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class ParameterDaoImplTest {

    @Autowired
    private ParameterDao parameterDao;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private ShopItemDao shopItemDao;


    public void setParameterDao(ParameterDao parameterDao) {
        this.parameterDao = parameterDao;
    }

    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public void setCategoryDao(CategoryDao categoryDao) {
        this.categoryDao = categoryDao;
    }

    public void setShopItemDao(ShopItemDao shopItemDao) {
        this.shopItemDao = shopItemDao;
    }

  /*  @Test
    public void testFindAllParameterTypeNameSuccess() {
        List<ParameterType> parameterTypeList = parameterDao.findAllParameterType();
        assertEquals(2, parameterTypeList.size()); // all found
        assertEquals("Строка", parameterTypeList.get(0).getParameterTypeName()); // right order
        assertEquals("Число", parameterTypeList.get(1).getParameterTypeName()); // right order
    }*/

    @Test
    public void testSaveOrUpdateParameterSuccess(){

    }



   /* @Test
    public void testAddParameterSuccess(){
        Parameter parameter = getParameter();
        assertTrue(parameter.getIdParameter() == 0);

        parameterDao.saveOrUpdate(parameter);
        assertEquals(true,parameterDao != null);
        assertTrue(parameter.getIdParameter() != 0);
    }*/
/*
    private Parameter getParameter(){
        ParameterType parameterType = new ParameterType();
        parameterType.setParameterTypeName("Тестовый Тип");

        Parameter parameter = new Parameter();
        parameter.setParameterName("Тест");
        parameter.setParameterOrdinal(1);
        parameter.setParameterType(parameterDao.findAllParameterType().get(0)); // String
        parameter.setCategory(categoryDao.findById(1)); //Some Category;
        return parameter;
    }*/

    @Test(expected = IllegalArgumentException.class)
    public void testAddNullParameterFail(){
        Parameter parameter = null;
        parameterDao.saveOrUpdate(parameter);
        assert false;
    }

   /* @Test(expected = IllegalArgumentException.class)
    public void testAddParameterWithNullParameterTypeFail(){
        Parameter parameter = getParameter();
        parameter.setParameterType(null);
        parameterDao.saveOrUpdate(parameter);
        assert false;
    }*/

  /*  @Test(expected = IllegalArgumentException.class)
    public void testAddParameterWithNullCategoryFail(){
        Parameter parameter = getParameter();
        parameter.setCategory(null);
        parameterDao.saveOrUpdate(parameter);
        assert false;
    }*/

  /*  @Test
    public void testAddParameterListSuccess(){
        List<Parameter> testList = new ArrayList<>();
        testList.add(getParameter());
        testList.get(0).setParameterName("List Param 0");
        testList.add(getParameter());
        testList.get(1).setParameterName("List Param 1");
        List<Parameter> resultList = parameterDao.addParameterList(testList);
        assertEquals("List Param 0", resultList.get(0).getParameterName());
        assertEquals(2,resultList.size());
    }*/
    @Test(expected = IllegalArgumentException.class)
    public void testAddNullParameterListFail(){
        List<Parameter> resultList = parameterDao.addParameterList(null);
        assert false;
    }

  /*  @Test
    public void addParameterValueOK(){
        ParameterValue value = getParameterValue();
        assertEquals(0, value.getIdParameterValue());
        parameterDao.saveOrUpdateParameterValue(value);
        assertTrue(value.getIdParameterValue()!=0);
    }*/

    private ParameterValue getParameterValue(){
        ShopItem shopItem = shopItemDao.findById(1);
        List<Parameter> listParameter = parameterDao.findAllParametersForCategoryOrdered(shopItem.getCategory());
        ParameterValue value = new ParameterValue();
        value.setShopItem(shopItem);
        value.setParameter(listParameter.get(0));
        value.setParameterValue("1");
        return value;
    }

 /*   @Test(expected = IllegalArgumentException.class)
    public void testAddNullParameterValueFail(){
        parameterDao.saveOrUpdateParameterValue(null);
        assert false;
    }*/

  /*  @Test(expected = IllegalArgumentException.class)
    public void testAddParameterValueWithNullShopItemFail(){
        ParameterValue value = getParameterValue();
        value.setShopItem(null);
        parameterDao.saveOrUpdateParameterValue(value);
        assert false;
    }*/
  /*  @Test(expected = IllegalArgumentException.class)
    public void testAddParameterValueWithNullParameterFail(){
        ParameterValue value = getParameterValue();
        value.setParameter(null);
        parameterDao.saveOrUpdateParameterValue(value);
        assert false;
    }*/

  /*  @Test
    public void testAddListParameterValuesSuccess(){
        List<ParameterValue> list = new ArrayList<>();
        ShopItem shopItem = shopItemDao.findById(1);
        List<Parameter> listParameter = parameterDao.findAllParametersForCategoryOrdered(shopItem.getCategory());
        ParameterValue value = new ParameterValue();
        value.setShopItem(shopItem);
        value.setParameter(listParameter.get(0));
        value.setParameterValue("1");
        list.add(value);
        ParameterValue value2 = new ParameterValue();
        value2.setShopItem(shopItem);
        value2.setParameter(listParameter.get(1));
        value2.setParameterValue("4");
        list.add(value2);
        parameterDao.addParameterValueList(list);
        assertEquals(2, list.size());
        assertTrue(list.get(1).getIdParameterValue()!=0);
    }*/

   /* @Test(expected = IllegalArgumentException.class)
    public void testAddNullListParameterValuesFail(){
        parameterDao.addParameterValueList(null);
        assert false;
    }*/

  /*  @Test
    public void testFindAllParametersValuesForShopItem(){
        List<ParameterValue> list = new ArrayList<>();
        ShopItem shopItem = shopItemDao.findById(1);
        List<Parameter> listParameter = parameterDao.findAllParametersForCategoryOrdered(shopItem.getCategory());
        ParameterValue value = new ParameterValue();
        value.setShopItem(shopItem);
        value.setParameter(listParameter.get(0));
        value.setParameterValue("1");
        list.add(value);
        ParameterValue value2 = new ParameterValue();
        value2.setShopItem(shopItem);
        value2.setParameter(listParameter.get(1));
        value2.setParameterValue("4");
        list.add(value2);
        parameterDao.addParameterValueList(list);
        List<ParameterValue> resultList = parameterDao.findAllParametersValuesForShopItem(shopItem);
        assertEquals(2, resultList.size());
        assertEquals("4", resultList.get(1).getParameterValue());
        assertEquals("1", resultList.get(0).getParameterValue());
    }*/

    @Test
    public void findAllParametersForCategoryAsDtoSuccess(){
        Category category  = categoryDao.findById(5);
        List<ParameterDto> list = parameterDao.findAllParametersForCategoryAsDto(category);
        assertEquals(2, list.size());
        assertEquals(5, list.get(0).getCategoryId());
        assertEquals(1, list.get(0).getParameterOrdinal());
        assertEquals(2, list.get(1).getParameterOrdinal());
    }



}
