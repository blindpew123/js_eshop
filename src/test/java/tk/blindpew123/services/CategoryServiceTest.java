package tk.blindpew123.services;


import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.services.dto.CategoryDto;
import tk.blindpew123.services.dto.DeleteCategoryDto;
import tk.blindpew123.services.entities.Category;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class CategoryServiceTest {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SessionFactory sessionFactory;




    /**
     *  Test getAllCategoriesAsDtoList() method with right results
     *  No marked as deleted Categories, right order Categories in list.
     */
    @Test
    public void testGetAllCategoriesAsDtoList_Success(){
        List<CategoryDto> categoryDtoList = categoryService.getAllCategoriesAsDtoList();
        assertNotNull(categoryDtoList);
        assertEquals(5, categoryDtoList.size());
        assertEquals("Dices",categoryDtoList.get(3).getCategoryName());
        assertEquals("Sleeves",categoryDtoList.get(4).getCategoryName());
    }

    /**
     *  Test saveOrUpdate method. Saving new Category with success result
     *  Category fields filled. Non-zero id.
     */
    @Test
    public void testSaveOrUpdate_SaveSuccess(){
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setCategoryName("Test");
        Category result = categoryService.saveOrUpdate(categoryDto);
        assertNotNull(result);
        assertEquals("Test", result.getCategoryName());
        assertNotEquals(0, result.getIdCategory());
    }
    /**
     *  Test saveOrUpdate method. Update existing Category with success result
     *  All fields matches
     */
    @Test
    public void testSaveOrUpdate_UpdateSuccess(){
        CategoryDto categoryDto = new CategoryDto();
        Category category = categoryService.getCategoryById(1);
        // in real situation session will be closed after getting dto
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        categoryDto.setIdCategory(category.getIdCategory());
        categoryDto.setCategoryNumber(category.getCategoryNumber());
        categoryDto.setCategoryName("Test");
        categoryDto.setParentId(0);
        categoryService.saveOrUpdate(categoryDto);
        sessionFactory.getCurrentSession().flush(); // for reading results from db
        sessionFactory.getCurrentSession().clear();
        Category result = categoryService.getCategoryById(1);
        assertEquals(category.getIdCategory(), result.getIdCategory());
        assertEquals(category.getParent(), result.getParent());
        assertEquals(category.getCategoryNumber(), result.getCategoryNumber());
        assertEquals("Test", result.getCategoryName());
    }

    /**
     *  Fail when save Category without name. Validation error
     */
    @Test(expected = javax.validation.ConstraintViolationException.class)
    public void testSaveOrUpdate_saveFailDueValidationErrors(){
        CategoryDto categoryDto = new CategoryDto();
        categoryService.saveOrUpdate(categoryDto);
    }

    /**
     * Successfully finding Category by id
     */
    @Test
    public void testGetCategoryById_Success(){
        assertEquals(1, categoryService.getCategoryById(1).getIdCategory());
    }

    /**
     * Searching Category with id that not exist in db returns null
     */
    @Test
    public void testGetCategoryById_NullDueNonExistingId(){
        assertNull(categoryService.getCategoryById(1001));
    }

    /**
     * Successfully finding Category's name by id
     */
    @Test
    public void testGetCategoryNameById_Success(){
        assertEquals("Cards", categoryService.getCategoryNameById(1));
    }

    /**
     * Searching Category with id that not exist in db returns empty String
     */
    @Test
    public void testGetCategoryNameById_EmptyDueNonExistingId(){
        assertEquals("", categoryService.getCategoryNameById(1001));
    }

    /**
     * Returns  Children Categories for non-zero non-null id successfully
     */
    @Test
    public void testGetAllCategoriesForParent_SuccessForNumericNonZeroId(){
        List<Category> categoryList = categoryService.getAllCategoriesForParent(2L);
        assertNotNull(categoryList);
        assertEquals(2, categoryList.size());
    }

    /**
     * Returns  Children Categories for zero id successfully
     */
    @Test
    public void testGetAllCategoriesForParent_SuccessForZeroId(){
        List<Category> categoryList = categoryService.getAllCategoriesForParent(0L);
        assertNotNull(categoryList);
        assertEquals(2, categoryList.size());
    }

    /**
     * Returns  Empty List of Children Categories for non-exitsitng id successfully
     */
    @Test
    public void testGetAllCategoriesForParent_EmptyForNonExistingId(){
        List<Category> categoryList = categoryService.getAllCategoriesForParent(1001L);
        assertNotNull(categoryList);
        assertEquals(0, categoryList.size());
    }

    /**
     * Returns  Children Categories for null id successfully
     */
    @Test
    public void testGetAllCategoriesForParent_SuccessForNullId(){
        List<Category> categoryList = categoryService.getAllCategoriesForParent(null);
        assertNotNull(categoryList);
        assertEquals(2, categoryList.size());
    }

    /**
     * Returns list of Categories for building UI elements successfully
     */

    @Test
    public void testGetBreadcrumbsForId_Success(){
        List<Category> categoryList = categoryService.getBreadcrumbsForId(4L);
        assertNotNull(categoryList);
        assertEquals(2, categoryList.size());
    }

    /**
     * Deletes Category and its children from db
     *
     */

    @Test
    public void testDeleteCategory(){
        DeleteCategoryDto categoryDto = new DeleteCategoryDto();
        categoryDto.setIdCategory(2L);
        categoryService.deleteCategory(categoryDto);
        sessionFactory.getCurrentSession().flush(); // for reading results from db
        sessionFactory.getCurrentSession().clear();
        List<CategoryDto> result = categoryService.getAllCategoriesAsDtoList();
        assertEquals(2, result.size());
    }

}
