package tk.blindpew123.services;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class SecurityServiceTest {

    @Autowired
    private SecurityService securityService;

    @Test
    public void testAutoLogin(){
        securityService.autologin("1","0");
        assertEquals("1", securityService.findLoggedInUsername());
    }
}
