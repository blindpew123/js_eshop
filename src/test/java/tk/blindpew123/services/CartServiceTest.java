package tk.blindpew123.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.services.dto.CartItemDto;
import tk.blindpew123.services.dto.CartListDto;
import tk.blindpew123.services.entities.CartItem;

import java.security.Principal;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class CartServiceTest {

    @Autowired
    private CartService cartService;

    @Autowired
    private UserService userService;

    @Test
    public void testGetQuantity(){
        assertEquals(7,cartService.getQuantity(1));
    }

    @Test
    public void testAddItemFromDtoToCart(){
        CartItemDto cartItemDto = new CartItemDto();
        cartItemDto.setIdShopItem(1);
        cartItemDto.setPrice(1);
        cartItemDto.setQnty(1);
        cartItemDto.setShopItemName("Item");
        CartListDto cartListDto = new CartListDto();
        cartListDto.getCartItems().add(cartItemDto);
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return userService.findById(1).getEmailAsLogin();
            }
        };
        cartService.addItemFromDtoToCart(cartItemDto, cartListDto, principal);
        assertEquals(2, cartListDto.getCartItems().get(0).getIdCartItem());

    }

}
