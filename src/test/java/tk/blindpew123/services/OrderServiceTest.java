package tk.blindpew123.services;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import tk.blindpew123.services.dao.OrderDao;
import tk.blindpew123.services.dao.PaymentTypeDao;
import tk.blindpew123.services.dao.ShippingDao;
import tk.blindpew123.services.dto.*;
import tk.blindpew123.services.entities.Customer;
import tk.blindpew123.services.entities.Order;
import tk.blindpew123.services.entities.OrderStatus;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class OrderServiceTest {

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CartService cartService;

    @Autowired
    private OrderDao orderDao;


    /**
     * Successfully filled OrderProcessDto
     */

    @Test
    public void testGetOrderProcessDto_Success(){
        CartListDto cartListDto = new CartListDto();
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return userService.findById(1).getEmailAsLogin();
            }
        };

        OrderProcessDto orderProcessDto = orderService.getOrderProcessDto(cartListDto, principal);
        assertNotNull(orderProcessDto);
        assertNotNull(orderProcessDto.getCartListDto());
        assertNotNull(orderProcessDto.getCustomerAddresses());
        assertNotNull(orderProcessDto.getPaymentTypes());
        assertNotNull(orderProcessDto.getShippings());
        assertNotNull(orderProcessDto.getCustomer());
    }

    /**
     * Order not processed, there are binding errors
     */

    @Test
    public void testProcessOrder_NoOrderItems_OrderNullAndBindingError(){
        // Creating Cart Dto
        CartListDto cartListDto = new CartListDto();
        CartItemDto cartItemDto = new CartItemDto();
        cartItemDto.setIdShopItem(1);
        cartItemDto.setQnty(1);
        cartItemDto.setPrice(1);
        List<CartItemDto> cartItemDtoList = new ArrayList<>();
        cartItemDtoList.add(cartItemDto);
        cartService.saveChangedCartItemDtos(cartItemDtoList,userService.findById(1)); // to Db
       // cartListDto.getCartItems().add(cartItemDto);

        // user
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return userService.findById(1).getEmailAsLogin();
            }
        };

        // Order Process Dto
        OrderProcessDto orderProcessDto = orderService.getOrderProcessDto(cartListDto, principal);
        BindingResult bindingResult = new DataBinder(orderProcessDto).getBindingResult();
        orderProcessDto.setSelectedAddress(customerService.getAddressById(1).getIdAddress());
        orderProcessDto.setSelectedPayment(1);
        orderProcessDto.setShipping(1);
        Order order = orderService.processOrder(orderProcessDto, bindingResult, principal);
        assertNull(order);
    }

    /**
     * Order not processed, qty not enough
     */

    @Test
    public void testProcessOrder_NotEnoughOrderItemQty_OrderNull(){
        // Creating Cart Dto
        CartListDto cartListDto = new CartListDto();
        CartItemDto cartItemDto = new CartItemDto();
        cartItemDto.setIdShopItem(1);
        cartItemDto.setQnty(12);
        cartItemDto.setPrice(1);
        List<CartItemDto> cartItemDtoList = new ArrayList<>();
        cartItemDtoList.add(cartItemDto);
        cartService.saveChangedCartItemDtos(cartItemDtoList,userService.findById(1)); // to Db
        cartListDto.getCartItems().add(cartItemDto);

        // user
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return userService.findById(1).getEmailAsLogin();
            }
        };

        // Order Process Dto
        OrderProcessDto orderProcessDto = orderService.getOrderProcessDto(cartListDto, principal);
        BindingResult bindingResult = new DataBinder(orderProcessDto).getBindingResult();
        orderProcessDto.setSelectedAddress(customerService.getAddressById(1).getIdAddress());
        orderProcessDto.setSelectedPayment(1);
        orderProcessDto.setShipping(1);
        Order order = orderService.processOrder(orderProcessDto, bindingResult, principal);
        assertNull(order);
    }

    /**
     * Successfully processing order
     */
    @Test
    public void testProcessOrder_Success(){
        // Creating Cart Dto
        CartListDto cartListDto = new CartListDto();
        CartItemDto cartItemDto = new CartItemDto();
        cartItemDto.setIdShopItem(1);
        cartItemDto.setQnty(1);
        cartItemDto.setPrice(1);
        List<CartItemDto> cartItemDtoList = new ArrayList<>();
        cartItemDtoList.add(cartItemDto);
        cartService.saveChangedCartItemDtos(cartItemDtoList,userService.findById(1));
        cartListDto.getCartItems().add(cartItemDto);

        // user
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return userService.findById(1).getEmailAsLogin();
            }
        };

        // Order Process Dto
        OrderProcessDto orderProcessDto = orderService.getOrderProcessDto(cartListDto, principal);
        BindingResult bindingResult = new DataBinder(orderProcessDto).getBindingResult();
        orderProcessDto.setSelectedAddress(customerService.getAddressById(1).getIdAddress());
        orderProcessDto.setSelectedPayment(1);
        orderProcessDto.setShipping(1);
        Order order = orderService.processOrder(orderProcessDto, bindingResult, principal);
        assertNotNull(order);
        assert order.getIdOrder() > 0;
        assertEquals(customerService.getCustomerByUser(userService.findById(1)).getIdCustomer(),
                order.getCustomer().getIdCustomer());
        assertNotNull(order.getOrderItemList());
        assertEquals(1, order.getOrderItemList().get(0).getShopItem().getIdShopItem());
        assertEquals(1, order.getAddress().getIdAddress());
        assertEquals(1, order.getShipping().getIdShipping());
        assertEquals(1, order.getPaymentType().getIdPaymentType());
        assertEquals(1, order.getOrderStatus().getIdOrderStatus());
    }

    /**
     * Successfully get List of orders for customer
     */
    @Test
    public void testGetAllOrderForCustomer_Success(){
        List<Order> list = orderService.getAllOrderForCustomer(customerService.getCustomerByUser(userService.findById(1)));
        assertEquals(1,list.size());
    }


    @Test
    public void getOrdersHistoryDto_Success(){
        // user
        Principal principal = new Principal() {
            @Override
            public String getName() {
                return userService.findById(1).getEmailAsLogin();
            }
        };
        OrdersHistoryDto dto  = new OrdersHistoryDto();
        assertNotNull(orderService.getOrdersHistoryDto(dto, principal));
        assertEquals(1, dto.getOrderList().size());
    }

    /**
     * Getting List of order between two dates
     */

    @Test
    public void testGetOrdersBetweenDate_Success(){
        List<Order> orderList = orderService.getOrdersBetweenDate(
                LocalDateTime.of(2018,1,1,0,0,0),
                LocalDateTime.of(2018,11,1,0,0,0));

        assertEquals(2, orderList.size());
    }

    @Test
    public void  testGetOrderAdminDtoList_Success(){
        List<OrderAdminDto> dtoList = orderService.getOrderAdminDtoList(
                LocalDateTime.of(2018,1,1,0,0,0),
                LocalDateTime.of(2018,11,1,0,0,0));
        assertEquals(2, dtoList.size());
    }

    @Test
    public void testGetAllOrderStatuses_Success(){
        List<OrderStatus> statusList = orderService.getAllOrderStatuses();
        assertEquals(5, statusList.size());
    }

    @Test
    public void  testOrderStatusUpdate_Success(){
        OrderStatusUpdateDto statusUpdateDto = new OrderStatusUpdateDto();
        statusUpdateDto.setIdOrder(1);
        statusUpdateDto.setIdStatus(4);
        orderService.orderStatusUpdate(statusUpdateDto);
        assertEquals(4, orderDao.findById(1).getOrderStatus().getIdOrderStatus());
    }

    @Test
    public void testGetTotalForPeriod_Success(){
        assertEquals (0, Double.compare(160d, orderService.getTotalForPeriod(LocalDateTime.of(2018,1,1,0,0,0),
                LocalDateTime.of(2018,11,1,0,0,0))));
    }

}
