package tk.blindpew123.services;

import org.hibernate.cache.cfg.internal.AbstractDomainDataCachingConfig;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.services.dto.Top10CustomerDto;
import tk.blindpew123.services.dto.UserProfileEditDto;
import tk.blindpew123.services.entities.Address;
import tk.blindpew123.services.entities.Customer;
import tk.blindpew123.services.entities.User;


import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class CustomerServiceTest {

    @Autowired
    @Spy
    private CustomerService customerService;

    @Autowired
    private UserService userService;


    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }



    /**
     * Testing successfully finding Customer when he exist
     */


    @Test
    public void testFindCustomerByUser_SuccessForExistingCustomer() {
        User user = userService.findById(1);
        Customer customer = customerService.getCustomerByUser(user);
        assertNotNull(customer);
        assertEquals("Сергей", customer.getFirstName());
    }

    /**
     *  When there are not Customer for existing user, will be thrown NoSuchCustomerException.
     */

    @Test(expected = tk.blindpew123.exceptions.NoSuchCustomerException.class)
    public void testFindCustmerByUser_FailWithNoSuchCustomerException(){
        User user = userService.findById(2); //admin. He is not Customer
        Customer customer = customerService.getCustomerByUser(user);
        assert(false);
    }

    /**
     *  When there are not User, will be thrown NoSuchCustomerException.
     */

    @Test(expected = tk.blindpew123.exceptions.NoSuchCustomerException.class)
    public void testFindCustmerByUser_WhenNullFailWithNoSuchCustomerException(){
        Customer customer = customerService.getCustomerByUser(null);
        assert(false);
    }

    /**
     * Finds address when it exist for that id
     */
    @Test
    public void testGetAddressById_Success(){
        Address address = customerService.getAddressById(1);
        assertNotNull(address);
        assertEquals("198000", address.getPostCode());
    }

    /**
     *  Returns null when there are not address for that id
     */

    @Test
    public void testGetAddressById_NullWhenIdNotExist(){
        Address address = customerService.getAddressById(1001);
        assertNull(address);
    }

    /**
     * Returns filled Dto for existing Customer
     */
    @Test
    public void testGetDtoWithCustomerProfile_Filled(){
        UserProfileEditDto userProfileEditDto = new UserProfileEditDto();
        User user = userService.findById(1);
        customerService.getDtoWithCustomerProfile(userProfileEditDto, user);
        assertEquals("Сергей", userProfileEditDto.getFirstName());
        assertEquals(2, userProfileEditDto.getAddresses().size());
        assertNotNull(userProfileEditDto.getEmptyAddress());
    }


    /**
     * Returns filled Dto for non-existing Customer
     */
    @Test
    public void testGetDtoWithCustomerProfile_Empty(){
        UserProfileEditDto userProfileEditDto = new UserProfileEditDto();
        User user = userService.findById(2);
        customerService.getDtoWithCustomerProfile(userProfileEditDto, user);
        assertEquals("", userProfileEditDto.getFirstName());
        assertEquals(0, userProfileEditDto.getAddresses().size());
        assertNotNull(userProfileEditDto.getEmptyAddress());
    }

    /**
     *  Testing change First and Last names changing
     */

    @Test
    public void testSaveOrUpdateCustomerProfile_WithoutUserNameChange_Success(){
        UserProfileEditDto userProfileEditDto = new UserProfileEditDto();
        userProfileEditDto.setIdUser(1);
        userProfileEditDto.setFirstName("Gary");
        userProfileEditDto.setLastName("Smith");
        userProfileEditDto.setUserName(userService.findById(1).getEmailAsLogin());
        userProfileEditDto.setBirthDate(""); // it can't be null when form returns data
        userProfileEditDto.setNewPassword(""); // the same
      //  CustomerService mockService = Mockito.spy(customerService);
    //  doNothing().when(customerService).saveOrUpdateAddresses(anyList(), any(Customer.class));
        assertFalse(customerService.saveOrUpdateCustomerProfile(userProfileEditDto));
        Customer customer = customerService.getCustomerByUser(userService.findById(1));
        assertEquals("Gary", customer.getFirstName());
    }

    /**
     * Saving CustomerProfile and change his userName. Returns true for re-login
     */
    @Test
    public void testSaveOrUpdateCustomerProfile_ChangingUserName_Success(){
        UserProfileEditDto userProfileEditDto = new UserProfileEditDto();
        userProfileEditDto.setIdUser(1);
        userProfileEditDto.setFirstName("Gary");
        userProfileEditDto.setLastName("Smith");
        userProfileEditDto.setUserName("Gary");
        userProfileEditDto.setBirthDate(""); // it can't be null when form returns data
        userProfileEditDto.setNewPassword(""); // the same
    //  CustomerService mockService = Mockito.spy(customerService);
    //  doNothing().when(customerService).saveOrUpdateAddresses(anyList(), any(Customer.class));
        assert customerService.saveOrUpdateCustomerProfile(userProfileEditDto);
        Customer customer = customerService.getCustomerByUser(userService.findById(1));
        assertEquals("Gary", customer.getFirstName());
    }

    /**
     *  Testing change First and Last names changing
     */

    @Test
    public void testSaveOrUpdateCustomerProfile_CustomerNotExist_Success(){
        UserProfileEditDto userProfileEditDto = new UserProfileEditDto();
        userProfileEditDto.setIdUser(2);
        userProfileEditDto.setFirstName("Gary");
        userProfileEditDto.setLastName("Smith");
        userProfileEditDto.setUserName(userService.findById(2).getEmailAsLogin());
        userProfileEditDto.setBirthDate(""); // it can't be null when form returns data
        userProfileEditDto.setNewPassword(""); // the same
        //  CustomerService mockService = Mockito.spy(customerService);
        //  doNothing().when(customerService).saveOrUpdateAddresses(anyList(), any(Customer.class));
        assertFalse(customerService.saveOrUpdateCustomerProfile(userProfileEditDto));
        Customer customer = customerService.getCustomerByUser(userService.findById(2));
        assertEquals("Gary", customer.getFirstName());
    }

    /**
     *  Testing change password
     */

    @Test
    public void testSaveOrUpdateCustomerProfile_PasswordChanged_Success(){
        UserProfileEditDto userProfileEditDto = new UserProfileEditDto();
        userProfileEditDto.setIdUser(1);
        userProfileEditDto.setFirstName("Gary");
        userProfileEditDto.setLastName("Smith");
        userProfileEditDto.setNewPassword("22");
        userProfileEditDto.setUserName(userService.findById(1).getEmailAsLogin());
        userProfileEditDto.setBirthDate(""); // it can't be null when form returns data
        //  CustomerService mockService = Mockito.spy(customerService);
        //  doNothing().when(customerService).saveOrUpdateAddresses(anyList(), any(Customer.class));
        assertFalse(customerService.saveOrUpdateCustomerProfile(userProfileEditDto));
        Customer customer = customerService.getCustomerByUser(userService.findById(1));
        assertEquals("Gary", customer.getFirstName());
        assert (bCryptPasswordEncoder.matches("22",
                userService.findById(1).getPassword()));
    }

    /**
     * Resets primary flag for all addresses of Customer
     */
    @Test
    public void testClearPrimary_Success(){
        List<Address> list = customerService.getCustomerByUser(
                userService.findById(1)).getAddresses();
        customerService.clearPrimary(list);
        for(Address address: list){
            assertFalse(address.isPrimaryAdr());
        }
    }

    /**
     * Set existing Address as primary successfully
     */
    @Test
    public void testSetPrimary_Success(){
        Address address = customerService.getAddressById(1); // not primary
        assert customerService.setPrimary(address).contains("set as primary");
        assert address.isPrimaryAdr();
    }

    /**
     * error message will be returned in case null parameter
     */

    @Test
    public void testSetPrimary_FailDueToNullAddress(){
        assert customerService.setPrimary(null).equals("Address doesn't exist");
    }


    /**
     * Saving list of Addresses successfully
     */

    @Test
    public void  testSaveOrUpdateAddresses_Success(){
        Customer customer = customerService.getCustomerByUser(userService.findById(1));
        List<Address> addressList = customer.getAddresses();
        addressList.get(0).setPostCode("200000");
        Address address = new Address();
        address.setPostCode("50000");
        address.setCountry("Russia");
        address.setCity("Vyborg");
        address.setStreet("Lenina st.");
        address.setBuilding("1");
        addressList.add(address);
        customerService.saveOrUpdateAddresses(addressList, customer);
        List<Address> checkList = customer.getAddresses();
        assertEquals("200000", checkList.get(0).getPostCode());
        assertEquals("50000", checkList.get(2).getPostCode());
    }

    /**
     * Saving single Address
     */
    @Test
    public  void testSaveOrUpdateAddress_Success(){
        Address address = new Address();
        address.setPostCode("50000");
        address.setCountry("Russia");
        address.setCity("Vyborg");
        address.setStreet("Lenina st.");
        address.setBuilding("1");
        customerService.saveOrUpdateAddress(address, 1);
        assertEquals(3, customerService.getCustomerByUser(userService.findById(1)).getAddresses().size());
        assertEquals("50000",customerService.getCustomerByUser(userService.findById(1)).getAddresses().get(2).getPostCode());
    }

    /**
     * Returns  top Customers ordered successfully
     */

    @Test
    public void testGetTop10CustomerTotal(){
        List<Top10CustomerDto> list = customerService.getTop10CustomerTotal();
        assertEquals(2, list.size());
        assertEquals(0, Double.compare(0d, list.get(0).getCustTotal())); // check sum
    }

    /**
     * Not used address completely deletes from db
     */
    @Test
    public  void testRemoveAddress_NotUsed_Successfully(){
        Customer customer = customerService.getCustomerByUser(userService.findById(1));
        customerService.removeAddress(customer.getAddresses(),1);
        List<Address> addressList = customer.getAddresses();
        assertEquals(1, addressList.size());
        assertFalse(addressList.get(0).isPrimaryAdr());
        assertNull(customerService.getAddressById(2));
    }

    /**
     * Used address deletes only from user's address list
     */
    @Test
    public  void testRemoveAddress_Used_Successfully(){
        Customer customer = customerService.getCustomerByUser(userService.findById(1));
        customerService.removeAddress(customer.getAddresses(),0);
        List<Address> addressList = customer.getAddresses();
        assertEquals(1, addressList.size());
        assert addressList.get(0).isPrimaryAdr();
        assertNotNull(customerService.getAddressById(1));
    }

}
