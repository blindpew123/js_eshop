package tk.blindpew123.services;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import tk.blindpew123.services.dto.ParamNameParamValueDto;
import tk.blindpew123.services.dto.ParameterDto;
import tk.blindpew123.services.entities.Parameter;
import tk.blindpew123.services.entities.ParameterType;
import tk.blindpew123.services.entities.ParameterValue;
import tk.blindpew123.services.entities.ShopItem;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:META-INF/spring/rootContext.xml")
@Transactional
@WebAppConfiguration
public class ParameterServiceTest {

    @Autowired
    private ParameterService parameterService;

    @Autowired
    private ShopItemService shopItemService;



    @Test
    public void testGetParametersForCategoryAsDto_Success(){
        List<ParameterDto> parameterDto = parameterService.getParametersForCategoryAsDto(1);
        assertNotNull(parameterDto);
        assertEquals(3, parameterDto.size());
    }

    @Test
    public void testGetAllParameterTypes(){
        List<ParameterType> parameterTypes = parameterService.getAllParameterTypes();
        assertNotNull(parameterTypes);
        assertEquals(2, parameterTypes.size());
    }

    @Test
    public void testMapParameterDtoToParameter(){
        ParameterDto parameterDto = new ParameterDto();
        parameterDto.setCategoryId(1);
        parameterDto.setIdParameter(2);
        parameterDto.setParameterName("TestParam");
        parameterDto.setParameterOrdinal(4);
        parameterDto.setParameterTypeId(1);
        Parameter parameter = parameterService.mapParameterDtoToParameter(parameterDto);
        assertNotNull(parameter);
        assertEquals(1, parameter.getCategory().getIdCategory());
        assertEquals("TestParam", parameter.getParameterName());
        assertEquals(1, parameter.getParameterType().getIdParameterType());
    }

    @Test
    public void testGetAllParameterNamesForCategory_Success(){
        List<String> list = parameterService.getAllParameterNamesForCategory(1);
        assertNotNull(list);
        assertEquals(3, list.size());
    }

    @Test
    public void testFindAllParameterTypesForCategoryAsOrderedList_Success(){
        List<String> list = parameterService.findAllParameterTypesForCategoryAsOrderedList(1);
        assertNotNull(list);
        assertEquals(3, list.size());
    }

    @Test
    public void testGetParametersForCategoryOrdered_Success(){
        List<Parameter> list = parameterService.getParametersForCategoryOrdered(1);
        assertNotNull(list);
        assertEquals(3, list.size());
    }

    @Test
    public void testGetParameterValueByShopItemAndParameter_Success(){
        ShopItem shopItem = shopItemService.getShopItemById(1);
        Parameter parameter = parameterService.getParameterById(1);
        ParameterValue parameterValue = parameterService
                .getParameterValueByShopItemAndParameter(shopItem, parameter);
        assertNotNull(parameterValue);
        assertEquals("3", parameterValue.getParameterValue());
    }

    @Test
    public void testGetAllParameterNamesAndValues_Success(){
        ShopItem shopItem = shopItemService.getShopItemById(1);
        List<ParamNameParamValueDto> list = parameterService.getAllParameterNamesAndValues(shopItem);
        assertNotNull(list);
        assertEquals(3, list.size());
        assertEquals("3", list.get(0).getValue());
    }

    @Test
    public void testGetFullFieldAliasesList_Success(){
        List<String> aliases = parameterService.getFullFieldAliasesList(1);
        assertNotNull(aliases);
        assertEquals(11, aliases.size());
    }

    @Test
    public void testGetFullFieldTypesList_Success(){
        List<String> types = parameterService.getFullFieldTypesList(1);
        assertNotNull(types);
        assertEquals(11, types.size());
    }

    @Test
    public void testSaveOrUpdateParameterDtoList_Success(){
        List<ParameterDto> parameterDtoList = new ArrayList<>();
        ParameterDto parameterDto = new ParameterDto();
        parameterDto.setParameterTypeId(1);
        parameterDto.setParameterOrdinal(4);
        parameterDto.setParameterName("Param");
        parameterDto.setCategoryId(1);
        parameterDtoList.add(parameterDto);
        parameterService.saveOrUpdateParameterDtoList(parameterDtoList);
        List<String> list = parameterService.getAllParameterNamesForCategory(1);
        assertEquals(4, list.size());
        assertEquals("Param",list.get(3));

    }
}

