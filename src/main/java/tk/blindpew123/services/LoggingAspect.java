package tk.blindpew123.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;


@Aspect
public class LoggingAspect {
    private static final Logger logger = LogManager.getLogger(LoggingAspect.class.getName());

    @Before("execution(* tk.blindpew123..*(..)) && !execution(* get*(..)) && !execution(* tk.blindpew123.controllers..*(..))")
    public void logBefore(JoinPoint joinPoint) {
        logger.debug("");
        logger.debug(joinPoint.getTarget());
        logger.debug("entered: " + joinPoint.getSignature().getName());
        Object[] signatureArgs = joinPoint.getArgs();
        for (Object signatureArg: signatureArgs) {
            logger.debug(" Arg: " + signatureArg);
        }
    }

    @Before("execution(* tk.blindpew123.controllers..*(..))")
    public void logBeforeController(JoinPoint joinPoint) {
        logger.debug("----------------------------");
        logger.debug("Controller: " + joinPoint.getTarget());
        logger.debug("method: " + joinPoint.getSignature().getName());
        Object[] signatureArgs = joinPoint.getArgs();
        for (Object signatureArg: signatureArgs) {
            logger.debug("Arg: " + signatureArg);
        }
    }


    @AfterReturning(value = "execution(* tk.blindpew123..*(..)) && !execution(* get*(..))", returning = "returnValue")
    public void logReturning(JoinPoint joinPoint, Object returnValue){
        logger.debug("Method: " + joinPoint.getSignature().getName());
        logger.debug("returns :" + returnValue);
    }

    @After("execution(* tk.blindpew123..*(..)) && !execution(* get*(..))")
    public void logReturning(JoinPoint joinPoint){
        logger.debug(joinPoint.getTarget());
        logger.debug("Exited from method: " + joinPoint.getSignature().getName());
    }

    @AfterThrowing(value = "execution(* tk.blindpew123..*(..)))", throwing = "error")
    public void logReturning(JoinPoint joinPoint, Throwable error){
        logger.debug("Method: " + joinPoint.getSignature().getName());
        logger.debug("throws :" + error);
    }
}
