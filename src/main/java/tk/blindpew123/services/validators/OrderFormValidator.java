package tk.blindpew123.services.validators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import tk.blindpew123.services.CartService;
import tk.blindpew123.services.dto.OrderProcessDto;

@Service
public class OrderFormValidator implements Validator {

    private static final Logger logger = LogManager.getLogger(OrderFormValidator.class.getName());

    @Autowired
    private CartService cartService;

    @Override
    public boolean supports(Class<?> aClass) {
        return OrderProcessDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        OrderProcessDto cart = (OrderProcessDto) o;
        if (cart.getSelectedAddress() == 0){
            errors.rejectValue("selectedAddress", "NotSelected.Order.Address");
        }
        if (cart.getSelectedPayment() == 0) {
            errors.rejectValue("selectedPayment", "NotSelected.Order.Payment");
        }
        if (cart.getShipping() == 0) {
            errors.rejectValue("shipping", "NotSelected.Order.Shipping");
        }

        /*
        if (cart.getCustomer() == null || cart.getCustomer().getIdCustomer() == 0) {
            errors.rejectValue("customer", "Empty.Order.Customer");
        }*/

        if (cart.getCartListDto() == null
                || cart.getCartListDto().getCartItems() == null
                || cart.getCartListDto().getCartItems().size() == 0){
            errors.rejectValue("cartListDto", "Empty.Order.CartList");
        }
    }
}
