package tk.blindpew123.services.validators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import tk.blindpew123.services.CartService;
import tk.blindpew123.services.dto.CartListDto;

@Service
public class CartValidator implements Validator {

    private static final Logger logger = LogManager.getLogger(CartValidator.class.getName());

    @Autowired
    private CartService cartService;

    @Override
    public boolean supports(Class<?> aClass) {
        return CartListDto.class.equals(aClass);
    }

    @Override
    public void validate(final Object o, final Errors errors){
        CartListDto cart = (CartListDto) o;
        for (int i = 0; i < cart.getCartItems().size(); i++) {
            if (cart.getCartItems().get(i).getQnty() < 0) {
                errors.rejectValue("cartItems[" + i + "].qnty", "Min.cart.cartItems.qnty");
                logger.info("Cart validate: found error: qty less zero");
            }
            if (cartService.getQuantity(cart.getCartItems().get(i).getIdShopItem())
                    < cart.getCartItems().get(i).getQnty()) {
                errors.rejectValue("cartItems[" + i + "].qnty", "QtyErr.cart.cartItems.qnty",
                        new Object[]{"" + cartService.getQuantity(cart.getCartItems().get(i).getIdShopItem())}, null);
                logger.info("Cart validate: found error: Stock items not enough");
            }
        }
    }

    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }
}
