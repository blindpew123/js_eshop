package tk.blindpew123.services.validators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import tk.blindpew123.services.UserService;
import tk.blindpew123.services.dto.UserProfileEditDto;
import tk.blindpew123.services.entities.User;

@Service
public class UserProfileDtoValidator implements Validator {
    private static final Logger logger = LogManager.getLogger(UserProfileDtoValidator.class.getName());

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public boolean supports(final Class<?> aClass) {
        return UserProfileEditDto.class.equals(aClass);
    }

    @Override
    public void validate(final Object o, final Errors errors) {
        logger.debug("Customer profile Validation: ...");
        UserProfileEditDto userProfileDto = (UserProfileEditDto) o;
        User user  = userService.findById(userProfileDto.getIdUser());

        if (userProfileDto.getFirstName() == null || userProfileDto.getFirstName().isEmpty()) {
            errors.rejectValue("firstName", "Empty.Customer.firstName");
            logger.debug("Empty first name");
        }
        if (userProfileDto.getLastName() == null || userProfileDto.getLastName().isEmpty()) {
            errors.rejectValue("lastName", "Empty.Customer.lastName");
            logger.debug("Empty last name");
        }

        if (!user.getEmailAsLogin().equals(userProfileDto.getUserName())){
            if (!bCryptPasswordEncoder.matches(userProfileDto.getOldPassword(), user.getPassword())) {
                errors.rejectValue("oldPassword", "NotMatch.Customer.OldPassword");
            }
        }

        if (userProfileDto.getUserName() == null || userProfileDto.getUserName().isEmpty()) {
            errors.rejectValue("userName", "Empty.Customer.userName");
            logger.debug("Empty user name");
        } else {
            User anotherUser = userService.findByEmailAsLogin(userProfileDto.getUserName());
            if (userProfileDto.getUserName() !=  userService.findById(userProfileDto.getIdUser()).getEmailAsLogin()
                    && anotherUser != null && anotherUser.getUser() != userProfileDto.getIdUser()) {
                errors.rejectValue("userName", "Duplicate.userForm.userName");
            }
        }


        if (!(userProfileDto.getNewPassword() == null || userProfileDto.getNewPassword().isEmpty())) {
            if (!bCryptPasswordEncoder.matches(userProfileDto.getOldPassword(), user.getPassword())) {
                errors.rejectValue("oldPassword", "NotMatch.Customer.OldPassword");
                logger.debug("Wrong old password");            }

            if (!userProfileDto.getNewPassword().equals(userProfileDto.getNewPasswordConfirm())) {
                errors.rejectValue("newPasswordConfirm", "NotMatch.Customer.NewPassword");
                logger.debug("New password not match");
            }
        }

        for (int i = 0; i< userProfileDto.getAddresses().size(); i++) {
            if (userProfileDto.getAddresses().get(i).getCountry() == null
                    || userProfileDto.getAddresses().get(i).getCountry().isEmpty()) {
                errors.rejectValue("addresses[" + i + "].country", "Empty.Customer.Address.Country");
            }

            if (userProfileDto.getAddresses().get(i).getCity() == null
                    || userProfileDto.getAddresses().get(i).getCity().isEmpty()) {
                errors.rejectValue("addresses[" + i + "].city", "Empty.Customer.Address.City");
            }

            if (userProfileDto.getAddresses().get(i).getPostCode() == null
                    || userProfileDto.getAddresses().get(i).getPostCode().isEmpty()) {
                errors.rejectValue("addresses[" + i + "].postCode", "Empty.Customer.Address.PostCode");
            }

            if (userProfileDto.getAddresses().get(i).getStreet() == null
                    || userProfileDto.getAddresses().get(i).getStreet().isEmpty()) {
                errors.rejectValue("addresses[" + i + "].street", "Empty.Customer.Address.Street");
            }

            if (userProfileDto.getAddresses().get(i).getBuilding() == null
                    || userProfileDto.getAddresses().get(i).getBuilding().isEmpty()) {
                errors.rejectValue("addresses[" + i + "].building", "Empty.Customer.Address.Building");
            }
        }

        if (!userProfileDto.getEmptyAddress().checkEmpty()) {
            if (userProfileDto.getEmptyAddress().getCountry() == null
                    || userProfileDto.getEmptyAddress().getCountry().isEmpty()) {
                errors.rejectValue("emptyAddress.country", "Empty.Customer.Address.Country");
            }

            if (userProfileDto.getEmptyAddress().getCity() == null
                    || userProfileDto.getEmptyAddress().getCity().isEmpty()) {
                errors.rejectValue("emptyAddress.city", "Empty.Customer.Address.City");
            }

            if (userProfileDto.getEmptyAddress().getPostCode() == null
                    || userProfileDto.getEmptyAddress().getPostCode().isEmpty()) {
                errors.rejectValue("emptyAddress.postCode", "Empty.Customer.Address.PostCode");
            }

            if (userProfileDto.getEmptyAddress().getStreet() == null
                    || userProfileDto.getEmptyAddress().getStreet().isEmpty()) {
                errors.rejectValue("emptyAddress.street", "Empty.Customer.Address.Street");
            }

            if (userProfileDto.getEmptyAddress().getBuilding() == null
                    || userProfileDto.getEmptyAddress().getBuilding().isEmpty()) {
                errors.rejectValue("emptyAddress.building", "Empty.Customer.Address.Building");
            }
        }

    }
}
