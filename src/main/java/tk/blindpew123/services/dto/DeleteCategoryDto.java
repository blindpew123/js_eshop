package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class DeleteCategoryDto {
    private long idCategory;

    public DeleteCategoryDto(){}

    public long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(long idCategory) {
        this.idCategory = idCategory;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
