package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import tk.blindpew123.services.entities.Address;
import tk.blindpew123.services.entities.Customer;
import tk.blindpew123.services.entities.PaymentType;
import tk.blindpew123.services.entities.Shipping;

import java.util.List;

public class OrderProcessDto {
    private Customer customer;
    private long selectedAddress;
    private List<Address> customerAddresses;
    private long selectedPayment;
    private List<PaymentType> paymentTypes;
    private long shipping;
    private List<Shipping> shippings;
    private CartListDto cartListDto;



    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public long getSelectedAddress() {
        return selectedAddress;
    }

    public void setSelectedAddress(long selectedAddress) {
        this.selectedAddress = selectedAddress;
    }

    public List<Address> getCustomerAddresses() {
        return customerAddresses;
    }

    public void setCustomerAddresses(List<Address> customerAddresses) {
        this.customerAddresses = customerAddresses;
    }

    public long getSelectedPayment() {
        return selectedPayment;
    }

    public void setSelectedPayment(long selectedPayment) {
        this.selectedPayment = selectedPayment;
    }

    public List<PaymentType> getPaymentTypes() {
        return paymentTypes;
    }

    public void setPaymentTypes(List<PaymentType> paymentTypes) {
        this.paymentTypes = paymentTypes;
    }

    public long getShipping() {
        return shipping;
    }

    public void setShipping(long shipping) {
        this.shipping = shipping;
    }

    public List<Shipping> getShippings() {
        return shippings;
    }

    public void setShippings(List<Shipping> shippings) {
        this.shippings = shippings;
    }

    public CartListDto getCartListDto() {
        return cartListDto;
    }

    public void setCartListDto(CartListDto cartListDto) {
        this.cartListDto = cartListDto;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }

}
