package tk.blindpew123.services.dto;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import tk.blindpew123.services.entities.ShopItem;

import javax.persistence.Tuple;
import java.util.List;

public class SingleShopItemTupleDto {
    private ShopItem shopItem;
    private List<ParamNameParamValueDto> shopItemNamesAndValues;


    public ShopItem getShopItem() {
        return shopItem;
    }

    public void setShopItem(ShopItem shopItem) {
        this.shopItem = shopItem;
    }

    public List<ParamNameParamValueDto> getShopItemNamesAndValues() {
        return shopItemNamesAndValues;
    }

    public void setShopItemNamesAndValues(List<ParamNameParamValueDto> shopItemNamesAndValues) {
        this.shopItemNamesAndValues = shopItemNamesAndValues;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }

}
