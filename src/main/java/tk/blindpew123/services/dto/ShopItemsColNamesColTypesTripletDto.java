package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;

public class ShopItemsColNamesColTypesTripletDto {
    private List<Object[]> shopItemsList;
    private List<String> columnNames;
    private List<String> columnTypes;

    public List<String> getColumnNames() {
        return columnNames;
    }

    public void setColumnNames(List<String> columnNames) {
        this.columnNames = columnNames;
    }

    public List<Object[]> getShopItemsList() {
        return shopItemsList;
    }

    public void setShopItemsList(List<Object[]> shopItemsList) {
        this.shopItemsList = shopItemsList;
    }

    public List<String> getColumnTypes() {
        return columnTypes;
    }

    public void setColumnTypes(List<String> columnTypes) {
        this.columnTypes = columnTypes;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
