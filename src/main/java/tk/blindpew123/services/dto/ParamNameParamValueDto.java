package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class ParamNameParamValueDto {
    private String name;
    private Object value;

    public ParamNameParamValueDto(){}

    public ParamNameParamValueDto(String name, Object value){
        this.name = name;
        this.value = value;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
