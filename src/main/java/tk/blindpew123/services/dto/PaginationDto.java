package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class PaginationDto {
    private long currentStartRecord;
    private long pageSize;
    private long currentPage;
    private long maxPages;
    private String params;
    private long currentCategory;

    public long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(long currentPage) {
        this.currentPage = currentPage;
    }

    public long getMaxPages() {
        return maxPages;
    }

    public void setMaxPages(long maxPages) {
        this.maxPages = maxPages;
    }

    public long getCurrentStartRecord() {
        return currentStartRecord;
    }

    public void setCurrentStartRecord(long currentStartRecord) {
        this.currentStartRecord = currentStartRecord;
    }

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }

    public long getCurrentCategory() {
        return currentCategory;
    }

    public void setCurrentCategory(long currentCategory) {
        this.currentCategory = currentCategory;
    }
}
