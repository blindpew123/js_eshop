package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class ParameterDto {
    private long idParameter;
    private String parameterName;
    private long parameterTypeId;
    private long categoryId;
    private int parameterOrdinal;

    public ParameterDto(){}

    public ParameterDto(long idParameter,
                        String parameterName,
                        long categoryId,
                        int parameterOrdinal,
                        long parameterTypeId){
        this.idParameter = idParameter;
        this.parameterName = parameterName;
        this.parameterTypeId = parameterTypeId;
        this.categoryId = categoryId;
        this.parameterOrdinal = parameterOrdinal;
    }

    public long getIdParameter() {
        return idParameter;
    }

    public void setIdParameter(long idParameter) {
        this.idParameter = idParameter;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public long getParameterTypeId() {
        return parameterTypeId;
    }

    public void setParameterTypeId(long parameterTypeId) {
        this.parameterTypeId = parameterTypeId;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public int getParameterOrdinal() {
        return parameterOrdinal;
    }

    public void setParameterOrdinal(int parameterOrdinal) {
        this.parameterOrdinal = parameterOrdinal;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
