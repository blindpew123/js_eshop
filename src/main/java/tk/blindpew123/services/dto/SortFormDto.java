package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.List;

public class SortFormDto {
    private String sortName;
    private List<String> sortNamesList;

    public String getSortName() {
        return sortName;
    }

    public void setSortName(String sortName) {
        this.sortName = sortName;
    }

    public List<String> getSortNamesList() {
        return sortNamesList;
    }

    public void setSortNamesList(List<String> sortNamesList) {
        this.sortNamesList = sortNamesList;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }

}
