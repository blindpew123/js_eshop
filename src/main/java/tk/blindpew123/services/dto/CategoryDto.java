package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import tk.blindpew123.services.entities.Category;

public class CategoryDto {
    private long idCategory;
    private String categoryName;
    private long parentId;
    private long categoryNumber;


    public CategoryDto(final long idCategory, final String categoryName, final Category parent, final long categoryNumber){
        this.idCategory = idCategory;
        this.categoryName = categoryName;
        this.parentId = parent == null ? 0 : parent.getIdCategory();
        this.categoryNumber = categoryNumber;
    }

    public CategoryDto(){}

    public long getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(long idCategory) {
        this.idCategory = idCategory;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public long getCategoryNumber() {
        return categoryNumber;
    }

    public void setCategoryNumber(long categoryNumber) {
        this.categoryNumber = categoryNumber;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
