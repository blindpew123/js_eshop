package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class CategoryContentSummaryDto {
    private int childrenCount;
    private long shopItemsCount;

    public int getChildrenCount() {
        return childrenCount;
    }

    public void setChildrenCount(int childrenCount) {
        this.childrenCount = childrenCount;
    }

    public long getShopItemsCount() {
        return shopItemsCount;
    }

    public void setShopItemsCount(long shopItemsCount) {
        this.shopItemsCount = shopItemsCount;
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }
}
