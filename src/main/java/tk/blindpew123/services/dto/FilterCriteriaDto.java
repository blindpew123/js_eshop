package tk.blindpew123.services.dto;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FilterCriteriaDto {
    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldAlias() {
        return fieldAlias;
    }

    public void setFieldAlias(String fieldAlias) {
        this.fieldAlias = fieldAlias;
    }

    private String fieldType;
    private String fieldName;
    private String fieldAlias;
    private String value;
    private String mathCondition;


    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMathCondition() {
        return mathCondition;
    }

    public void setMathCondition(String mathCondition) {
        this.mathCondition = mathCondition;
    }

    public List<String> getListConditions(){
        return new ArrayList<String>(Arrays.asList("", ">", ">=", "<", "<=", "="));
    }

    @Override
    public String toString(){
        return ReflectionToStringBuilder.toString(this);
    }

}
