package tk.blindpew123.services.entities;

import javax.persistence.*;
import java.util.Objects;

@NamedQueries({
        @NamedQuery(name = "ParameterValue.deleteAllValuesForParameter",
        query = "DELETE ParameterValue v WHERE v.parameter =:p")
})

@Entity
public class ParameterValue {

    private long idParameterValue;
    private ShopItem shopItem;
    private Parameter parameter;
    private String parameterValue;

    @ManyToOne
    @JoinColumn(name = "ShopItem")
    public ShopItem getShopItem() {
        return shopItem;
    }

    public void setShopItem(ShopItem shopItem) {
        this.shopItem = shopItem;
    }

    @ManyToOne
    @JoinColumn(name = "Parameter")
    public Parameter getParameter() {
        return parameter;
    }

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    @Column(name = "ParameterValue")
    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getIdParameterValue() {
        return idParameterValue;
    }

    public void setIdParameterValue(long idParameterValue) {
        this.idParameterValue = idParameterValue;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append("ParameterValue {")
                .append("idParameterValue: ").append(idParameterValue).append(", ")
                .append("shopItem: ").append(shopItem == null ? "null" : shopItem.getItemName()).append(", ")
                .append("parameter: ").append(parameter == null ? "null" : parameter.getParameterName()).append(", ")
                .append("parameterValue").append(Objects.toString(parameterValue))
                .append('}').toString();
    }
}
