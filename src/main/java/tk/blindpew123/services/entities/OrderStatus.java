package tk.blindpew123.services.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NamedQueries({
        @NamedQuery(name = "OrderStats.findOrderStatusById",
                query = "FROM OrderStatus os WHERE os.id = :id"),
        @NamedQuery(name = "OrderStatus.getAllOrderStatuses",
                query = "FROM OrderStatus os")}
)
public class OrderStatus {

    public enum Status {
        AWAITING_PAYMENT(2),
        PROCESSING(1),
        SHIPPING(3),
        DELIVERED(4),
        RETURN(5);

        Status(int value) {
            this.value = value;
        }

        long value;

        public long getValue() {
            return this.value;
        }
    }

    private long idOrderStatus;
    private String orderStatusName;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdOrderStatus() {
        return idOrderStatus;
    }

    public void setIdOrderStatus(long idOrderStatus) {
        this.idOrderStatus = idOrderStatus;
    }

    @Column(name = "OrderStatusName")
    public String getOrderStatusName() {
        return orderStatusName;
    }

    public void setOrderStatusName(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        return sb.append("OrderStatus: {")
                .append("idOrderStatus: ").append(idOrderStatus).append(", ")
                .append("orderStatusName: ").append(orderStatusName)
                .append("}").toString();
    }
}
