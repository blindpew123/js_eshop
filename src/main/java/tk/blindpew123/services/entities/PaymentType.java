package tk.blindpew123.services.entities;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@NamedQueries(
        @NamedQuery(name = "PaymentType.findAllPaymentTypes",
            query = "FROM PaymentType p"))
public class PaymentType {

    public enum Payment{
        CASH(1),
        CARD(2);
        Payment(int value){
            this.value = value;
        }
        long value;
        public long getValue(){
            return this.value;
        }
    }



    private long idPaymentType;
    private String paymentName;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdPaymentType() {
        return idPaymentType;
    }

    public void setIdPaymentType(long idPaymentType) {
        this.idPaymentType = idPaymentType;
    }

    @Column(name = "PaymentName")
    public String getPaymentName() {
        return paymentName;
    }

    public void setPaymentName(String paymentName) {
        this.paymentName = paymentName;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        return sb.append("PaymentType: {")
                .append("idPaymentType: ").append(idPaymentType).append(", ")
                .append("paymentName: ").append(paymentName)
                .append("}").toString();
    }
}
