package tk.blindpew123.services.entities;

import javax.persistence.*;

import java.util.Objects;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "CartItem")
public class CartItem {

    private long idCartItem;
    private User user;
    private ShopItem shopItem;
    private int qnty;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdCartItem() {
        return idCartItem;
    }

    public void setIdCartItem(long idCartItem) {
        this.idCartItem = idCartItem;
    }

    @OneToOne(targetEntity = ShopItem.class)
    @JoinColumn(name = "shopItem")
    public ShopItem getShopItem() {
        return shopItem;
    }

    public void setShopItem(ShopItem shopItem) {
        this.shopItem = shopItem;
    }

    public int getQnty() {
        return qnty;
    }

    public void setQnty(int qnty) {
        this.qnty = qnty;
    }

    @ManyToOne
    @JoinColumn(name = "user")
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        return sb.append("CartItem: {").append("idCartItem: ").append(idCartItem).append(", ")
                .append("user(id): ").append(user == null? "null": user.getUser()).append(", ")
                .append("shopItem: ").append(shopItem == null ? "null": shopItem.getItemName())
                .append("Qty: ").append(qnty).append("}").toString();
    }
}
