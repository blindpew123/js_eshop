package tk.blindpew123.services.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
public class OrderItem {
    private long idOrderItem;
    private Order order;
    private ShopItem shopItem;
    private long qty;
    private double itemPrice;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    public long getIdOrderItem() {
        return idOrderItem;
    }

    public void setIdOrderItem(long idOrderItem) {
        this.idOrderItem = idOrderItem;
    }

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "custOrder")
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    @ManyToOne
    @JoinColumn(name = "ShopItem")
    public ShopItem getShopItem() {
        return shopItem;
    }

    public void setShopItem(ShopItem shopItem) {
        this.shopItem = shopItem;
    }

    @Column(name = "Qty")
    public long getQty() {
        return qty;
    }

    public void setQty(long qty) {
        this.qty = qty;
    }

    @Column(name = "ItemPrice")
    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        return sb.append("OrderItem: {")
                .append("idOrderItem: ").append(idOrderItem).append(", ")
                .append("order(id): ").append(order == null ? "null" : order.getIdOrder()).append(", ")
                .append("shopItem: ").append(shopItem == null ? "null" : shopItem.getItemName())
                .append("Qty: ").append(qty).append(", ")
                .append("itemPrice: ").append(itemPrice)
                .append("}").toString();
    }
}
