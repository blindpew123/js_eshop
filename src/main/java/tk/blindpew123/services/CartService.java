package tk.blindpew123.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import tk.blindpew123.services.dao.CartItemDao;
import tk.blindpew123.services.dto.CartItemDto;
import tk.blindpew123.services.dto.CartListDto;
import tk.blindpew123.services.entities.CartItem;
import tk.blindpew123.services.entities.ShopItem;
import tk.blindpew123.services.entities.User;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.security.Principal;
import java.util.*;

@Service
public class CartService {

    private static final Logger logger = LogManager.getLogger(CartService.class.getName());

    @Autowired
    private ShopItemService shopItemService;
    @Autowired
    private UserService userService;
    @Autowired
    private CartItemDao cartItemDao;

    @Transactional
    public String getShopItemName(final long id) {
        return shopItemService.getShopItemById(id).getItemName();
    }

    @Transactional
    public int getQuantity(final long id) {
        ShopItem shopItem = shopItemService.getShopItemById(id);
        return shopItem == null ? 0 : shopItem.getQnty();
    }

    @Transactional
    public void addItemFromDtoToCart(final CartItemDto cartItemDto,
                                     final CartListDto cartListDto,
                                     final Principal principal) {
        if (cartItemDto.getIdShopItem() != 0) {  // Can't add 0!. For remove - use cart controls
            boolean isAlreadyAvailable = false; // may be cart contains this item already
            for (CartItemDto oldCartItem : cartListDto.getCartItems()) {
                if (oldCartItem.getIdShopItem() == cartItemDto.getIdShopItem()) {
                    isAlreadyAvailable = true;
                    oldCartItem.setQnty(oldCartItem.getQnty() + cartItemDto.getQnty());
                    break;
                }
            }

            if (!isAlreadyAvailable) {
                cartItemDto.setShopItemName(shopItemService.getShopItemById(cartItemDto.getIdShopItem()).getItemName());
                cartListDto.getCartItems().add(cartItemDto);
            }

            mergeCart(cartListDto, principal);

         /*   if (principal != null) { // only for user
                User user = userService.findByEmailAsLogin(principal.getName());
                Hibernate.initialize(user.getCartItems());
                CartItem cartItem = mapCartItemDtoToCartItem(cartItemDto);
                if (cartItem.getUser() != null) { //TODO: check the same user!
                    cartItem.setQnty(cartItem.getQnty() + cartItemDto.getQnty());
                } else {
                    cartItem.setQnty(cartItemDto.getQnty());
                    cartItem.setUser(user);
                    cartItemDao.saveOrUpdate(cartItem);
                    user.getCartItems().add(cartItem);
                    cartItemDto.setIdCartItem(cartItem.getIdCartItem());
                }
            } */
        }
        updateTotal(cartListDto);
    }


    @Transactional
    public void mergeCart(final CartListDto cartListDto, final Principal principal) {

        if (principal == null) {
            for (int i = 0; i < cartListDto.getCartItems().size(); i++) {
               if (cartListDto.getCartItems().get(i).getQnty() <= 0) { // remove from cart
                    cartListDto.getCartItems().remove(i--);
                    continue;
                }
                //UpdatePrice
                Double price = shopItemService.getShopItemById(
                        cartListDto.getCartItems().get(i).getIdShopItem()).getPrice();
                if (Double.compare(price, 0d) == 0){
                    cartListDto.getCartItems().remove(i--);
                    continue;
                } else {
                    cartListDto.getCartItems()
                            .get(i).setPrice(price);
                }
            }
            updateTotal(cartListDto);
            return;
        }

        User user = userService.findByEmailAsLogin(principal.getName());
        Hibernate.initialize(user.getCartItems());
        Map<Long, CartItemDto> map = new HashMap<>();  //for comparing current cart with saved items
        Deque<CartItem> toDelete = new ArrayDeque<>();
        for (CartItemDto cartItemDto : cartListDto.getCartItems()) {
            map.put(cartItemDto.getIdShopItem(), cartItemDto);
        }

        for (CartItem cartItem : user.getCartItems()) {
            if (map.containsKey(cartItem.getShopItem().getIdShopItem())) {
                // we have same items, but...
                if (map.get(cartItem.getShopItem().getIdShopItem()).getIdCartItem() == 0) {
                    // it is from different user state - merge qty
                    map.get(cartItem.getShopItem().getIdShopItem())
                            .setIdCartItem(cartItem.getIdCartItem()); // same Item
                    map.get(cartItem.getShopItem().getIdShopItem())
                            .setQnty(map.get(cartItem.getShopItem().getIdShopItem()).getQnty() + cartItem.getQnty());
                } else { // or set last qty
                    if (map.get(cartItem.getShopItem().getIdShopItem()).getQnty() > 0) {
                        if (map.get(cartItem.getShopItem().getIdShopItem()).getQnty()
                                != cartItem.getQnty()) {
                            cartItem.setQnty(map.get(cartItem.getShopItem().getIdShopItem()).getQnty());
                        }
                    } else { // qnty == 0, user wants remove item
                        toDelete.push(cartItem);
                        logger.info("Remove from dto cart " + map.get(cartItem.getShopItem().getIdShopItem()));
                        logger.info(cartListDto.getCartItems().size());
                        cartListDto.getCartItems().remove(map.get(cartItem.getShopItem().getIdShopItem()));
                        logger.info(cartListDto.getCartItems().size());
                    }
                }
                map.remove(cartItem.getShopItem().getIdShopItem()); // processed

            } else { // not in current cart, but user had it before. Just add
                cartListDto.getCartItems().add(mapCartItemToDto(cartItem));
            }
        }

        while (toDelete.peek() != null) {
            CartItem cartItem = toDelete.pop();
            cartItemDao.delete(cartItem);
            user.getCartItems().remove(cartItem);
        }


        saveChangedCartItemDtos(map.values(), user);
        deleteItemsWithZeroPrice(cartListDto);
        updateTotal(cartListDto);
    }

    // Saving all changes
    @Transactional
    protected void saveChangedCartItemDtos(Collection<CartItemDto> cartItemDtos, User user) {

        cartItemDtos.stream().forEach(cartItemDto -> {
            CartItem cartItem = mapCartItemDtoToCartItem(cartItemDto);
            cartItem.setUser(user);
            cartItemDao.saveOrUpdate(cartItem);
            cartItemDto.setIdCartItem(cartItem.getIdCartItem());
            // and update price
            cartItemDto.setPrice(shopItemService.getShopItemById(
                    cartItemDto.getIdShopItem()).getPrice());
        });

    }

    protected void deleteItemsWithZeroPrice(CartListDto cartListDto){
        cartListDto.getCartItems().forEach(cartItemDto -> {
            cartItemDto.setPrice(shopItemService.getShopItemById(cartItemDto.getIdShopItem()).getPrice());
            if(Double.compare(cartItemDto.getPrice(), 0d) == 0){
                cartItemDao.delete(cartItemDao.findById(cartItemDto.getIdCartItem()));
                cartItemDto.setIdCartItem(0);
            }
        });
        cartListDto.getCartItems().removeIf(cartItemDto ->
                cartItemDto.getIdCartItem() == 0);
    }

       /* for (int i = 0; i <  cartItemDtos.size(); i++) {
            CartItem cartItem = mapCartItemDtoToCartItem(cartItemDtos.get);
            cartItem.setUser(user);
            cartItemDao.saveOrUpdate(cartItem);
            cartItemDto.setIdCartItem(cartItem.getIdCartItem());
            // and update price
            cartItemDto.setPrice(shopItemService.getShopItemById(
                    cartItemDto.getIdShopItem()).getPrice());
        }*/


    protected CartItem mapCartItemDtoToCartItem(CartItemDto cartItemDto) {
        CartItem cartItem = null;
        if (cartItemDto.getIdCartItem() != 0) {
            cartItem = cartItemDao.findById(cartItemDto.getIdCartItem());
        } else {
            cartItem = new CartItem();
            cartItem.setQnty(cartItemDto.getQnty());
            cartItem.setShopItem(shopItemService.getShopItemById(cartItemDto.getIdShopItem()));
        }
        return cartItem;
    }

    protected CartItemDto mapCartItemToDto(CartItem cartItem) {
        CartItemDto cartItemDto = new CartItemDto();
        cartItemDto.setShopItemName(cartItem.getShopItem().getItemName());
        cartItemDto.setPrice(cartItem.getShopItem().getPrice());
        cartItemDto.setIdCartItem(cartItem.getIdCartItem());
        cartItemDto.setIdShopItem(cartItem.getShopItem().getIdShopItem());
        cartItemDto.setQnty(cartItem.getQnty());
        cartItemDto.setShopItemSmallImg(null); //TODO: when images will be work
        return cartItemDto;
    }

    @Transactional
    public CartListDto deletePositionFromCart(final CartListDto cartListDto, final long idShopItem) {

        for (int i = 0; i < cartListDto.getCartItems().size(); i++) {
            if (cartListDto.getCartItems().get(i).getIdShopItem() == idShopItem) {
                if (cartListDto.getCartItems().get(i).getIdCartItem() != 0) {
                    cartItemDao.delete(
                            cartItemDao.findById(
                                    cartListDto.getCartItems().get(i).getIdCartItem()));
                }
                cartListDto.getCartItems().remove(i);
            }
        }
        updateTotal(cartListDto);
        return cartListDto;
    }


    public void addResultPreviousCartCheckingToModel(final HttpServletRequest request, final Model model) {
        BindingResult cartBind = (BindingResult) request.getSession().getAttribute("cartFormBinding");
        if (cartBind != null && cartBind.hasErrors()) {
            model.addAttribute("org.springframework.validation.BindingResult.cart", cartBind);
        }
    }

    @Transactional()
    public void deleteCartItemById(final long id){
        cartItemDao.delete(cartItemDao.findById(id));
    }

    @Transactional
    public void updateTotal(final CartListDto cartListDto){
        cartListDto.setTotal(0);
        cartListDto.getCartItems().stream().forEach(cartItemDto -> {
            cartListDto.setTotal(cartListDto.getTotal()
                    + cartItemDto.getQnty() * cartItemDto.getPrice());
        });
    }

}
