package tk.blindpew123.services.dao;


import tk.blindpew123.services.dto.ParamNameParamValueDto;
import tk.blindpew123.services.dto.ParameterDto;
import tk.blindpew123.services.entities.*;

import javax.persistence.Tuple;
import java.util.List;

public interface
ParameterDao extends GenericDao<Parameter>{        // Тоже для списка


    List<Parameter> addParameterList(List<Parameter> parameterList);
    // Записать значение параметра
    // Получить список параметров для конкретной Категории отсортировнный по порядковому номеру
    List<Parameter> findAllParametersForCategoryOrdered(Category category);
    // Получить список параметров для конкретной Категории в виде Dto
    List<ParameterDto> findAllParametersForCategoryAsDto(Category category);
    // Получить список параметров со значенями для товара

    List<ParamNameParamValueDto> findAllParameterNamesAndValuesOrderedByOrdinalForShopItem(ShopItem shopItem);
    // Найти все Id Товаров Для шаблона поиска - название категории - значение(список шаблонов)
    List<ShopItem> findAllShopItemForCriteriesList(List<ParameterValue> parameterValueList);

    // Получить все Параметры
    List<Parameter> findAllParameters();
    //
    List<String> findAllParameterNamesForCategory(Category category);

    ParameterValue findParameterValueByShopItemAndParameter(ShopItem shopItem, Parameter parameter);
}
