package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.OrderItem;

public interface OrderItemDao extends GenericDao<OrderItem>  {
}
