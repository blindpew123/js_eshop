package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.Role;
import tk.blindpew123.services.entities.User;

import java.util.HashSet;

public interface UserDao extends GenericDao<User> {
    public User findUserByLogin(String emailAsLogin);
    public User saveOrUpdate(User user);
    public HashSet<Role> findUserRoleAsUser();
    public HashSet<Role> findUserRoleAsAdmin();
}
