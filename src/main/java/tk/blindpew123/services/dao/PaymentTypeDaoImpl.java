package tk.blindpew123.services.dao;


import org.springframework.stereotype.Service;
import tk.blindpew123.services.dao.GenericDaoImpl;
import tk.blindpew123.services.dao.PaymentTypeDao;
import tk.blindpew123.services.entities.PaymentType;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class PaymentTypeDaoImpl extends GenericDaoImpl<PaymentType> implements PaymentTypeDao {

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<PaymentType> findAll() {
        return sessionFactory.getCurrentSession().createNamedQuery("PaymentType.findAllPaymentTypes").getResultList();
    }
}
