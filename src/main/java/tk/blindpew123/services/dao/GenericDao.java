package tk.blindpew123.services.dao;

import java.util.List;

public interface GenericDao<T> {
    T saveOrUpdate(T t);
    default void delete(T t){
        throw new UnsupportedOperationException("Delete operation not implemented");
    }
    T findById(long id);
    default List<T> findAll(){
        throw new UnsupportedOperationException("Find All operation not implemented");
    }
}
