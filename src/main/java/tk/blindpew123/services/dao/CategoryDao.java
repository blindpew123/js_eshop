package tk.blindpew123.services.dao;

import tk.blindpew123.models.CategoryManagementModel;
import tk.blindpew123.services.dto.CategoryDto;
import tk.blindpew123.services.entities.Category;

import java.util.List;

/**
 *  Methods to execute common operations for shop's data administration
 *  User have to create queries to DB for related operation.
 */

public interface CategoryDao extends GenericDao<Category> {

    /**
     * Deletes category from database
     * @param category Category object to be deleted.
     * @param isWithOrphans true if orphans need be deleted too (optional)
     */
    @Deprecated
    void deleteCategory(Category category, boolean... isWithOrphans);

    /**
     * Retrieves all available categories as Dto
     * @return all categories as List<CategoryDto>
     */
    List<CategoryDto> findAllCategoriesAsDtoList();

    /**
     * Updates ordinal of category with common Parent, starting with pointed position
     * incrementing or decrementing depends on the step
     * @param commonParent Parent for searching common children
     * @param after starting position (excluded)
     * @param step negative or positive number that will be added to current position
     */
    void renumberSameLevelCategories(Category commonParent, long after, long step);

    List<Category> findAllCatgoriesForParent(Category parent);

    void delete(List<Category> categoriesForDelete);

}
