package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.Address;

public interface AddressDao extends GenericDao<Address> {
}
