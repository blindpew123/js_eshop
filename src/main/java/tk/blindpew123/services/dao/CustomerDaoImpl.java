package tk.blindpew123.services.dao;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.blindpew123.exceptions.NoSuchCustomerException;
import tk.blindpew123.services.dto.Top10CustomerDto;
import tk.blindpew123.services.entities.Address;
import tk.blindpew123.services.entities.Customer;
import tk.blindpew123.services.entities.Order;
import tk.blindpew123.services.entities.User;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CustomerDaoImpl extends GenericDaoImpl<Customer> implements CustomerDao {

    @Transactional(value = Transactional.TxType.MANDATORY, dontRollbackOn = NoSuchCustomerException.class)
    @Override
    public Customer findCustomerByUser(final User user) {
        Query<Customer> customerQuery = sessionFactory.getCurrentSession()
                .createNamedQuery("Customer.findCustomerByUser");
        List<Customer> listCustomer = customerQuery.setParameter("user", user).getResultList();
        if (listCustomer.size() == 0) {
            NoSuchCustomerException e = new NoSuchCustomerException();
            e.setUser(user);
            throw e;
        }
        return listCustomer.get(0);
    }

    //TODO: Move to Order Dao
    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<Order> findOrdersWithAddress(final Address address){
        return sessionFactory.getCurrentSession()
                .createNamedQuery("Order.findOrderWithAddress", Order.class)
                .setParameter("a", address).getResultList();
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<Top10CustomerDto> findTop10CustomerTotal() {
        return sessionFactory.getCurrentSession()
                .createNamedQuery("Customer.Top10Customer")
                .getResultList();

    }
}
