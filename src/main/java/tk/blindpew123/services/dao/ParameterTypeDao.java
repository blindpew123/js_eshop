package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.Category;
import tk.blindpew123.services.entities.ParameterType;

import java.util.List;

public interface ParameterTypeDao extends GenericDao<ParameterType> {
    List<ParameterType> findAllParameterType();
    List<String> findAllParameterTypesForCategoryAsOrderedList(Category category);
}
