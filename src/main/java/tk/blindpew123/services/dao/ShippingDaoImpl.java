package tk.blindpew123.services.dao;

import org.springframework.stereotype.Service;
import tk.blindpew123.services.entities.Shipping;

import javax.transaction.Transactional;
import java.util.List;


@Service
public class ShippingDaoImpl extends GenericDaoImpl<Shipping> implements ShippingDao{

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public List<Shipping> findAll(){
        return sessionFactory.getCurrentSession().createNamedQuery("Shipping.findAllShipping").getResultList();
    }
}
