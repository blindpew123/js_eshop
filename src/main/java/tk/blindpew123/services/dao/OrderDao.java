package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface OrderDao extends GenericDao<Order>{

    List<Order> findAllOrdersBetweenDates(LocalDateTime startDate, LocalDateTime endDate);
    List<Order> findAllOrderByCustomer(Customer customer);
    Double findTotalForPeriod(LocalDateTime startDate, LocalDateTime endDate);
}
