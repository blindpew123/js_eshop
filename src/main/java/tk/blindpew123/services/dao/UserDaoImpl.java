package tk.blindpew123.services.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jpa.QueryHints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.blindpew123.services.entities.*;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;

@Service
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {


    @Transactional(value = Transactional.TxType.MANDATORY)
    @Override
    public User findUserByLogin(final String emailAsLogin) {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = builder.createQuery(User.class);
        Root<User> root = criteriaQuery.from(User.class);
        root.fetch(User_.roles, JoinType.LEFT);
        criteriaQuery.select(root);
        criteriaQuery.where(builder.equal(root.get(User_.emailAsLogin), emailAsLogin));
    //  criteriaQuery.distinct(true);
        List<User> resultList =  sessionFactory.getCurrentSession()
                .createQuery(criteriaQuery)
        //      .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .getResultList();
        return resultList.size() == 0 ? null : resultList.get(0);
    }

    @Override
    public HashSet<Role> findUserRoleAsUser() {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Role> criteriaQuery = builder.createQuery(Role.class);
        Root<Role> root = criteriaQuery.from(Role.class);
        criteriaQuery.select(root);
        criteriaQuery.where(builder.equal(root.get(Role_.name), "ROLE_USER"));
        List<Role> result  = sessionFactory.getCurrentSession().createQuery(criteriaQuery).getResultList();
        return new HashSet<>(result);
    }

    @Override
    public HashSet<Role> findUserRoleAsAdmin() {
        CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
        CriteriaQuery<Role> criteriaQuery = builder.createQuery(Role.class);
        Root<Role> root = criteriaQuery.from(Role.class);
        criteriaQuery.select(root);
        criteriaQuery.where(builder.equal(root.get(Role_.name), "ROLE_ADMIN"));
        List<Role> result  = sessionFactory.getCurrentSession().createQuery(criteriaQuery).getResultList();
        return new HashSet<>(result);
    }


}
