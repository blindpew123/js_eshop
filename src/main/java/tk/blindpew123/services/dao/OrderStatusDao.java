package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.OrderStatus;

public interface OrderStatusDao extends GenericDao<OrderStatus> {
    default OrderStatus saveOrUpdate(OrderStatus paymentType){
        throw new UnsupportedOperationException("SaveOrUpdate not implemented");
    }
}
