package tk.blindpew123.services.dao;

import tk.blindpew123.services.entities.CartItem;

public interface CartItemDao extends GenericDao<CartItem> {
}
