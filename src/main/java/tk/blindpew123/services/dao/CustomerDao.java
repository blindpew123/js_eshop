package tk.blindpew123.services.dao;

import tk.blindpew123.services.dto.Top10CustomerDto;
import tk.blindpew123.services.entities.Address;
import tk.blindpew123.services.entities.Customer;
import tk.blindpew123.services.entities.Order;
import tk.blindpew123.services.entities.User;

import java.util.List;

public interface CustomerDao extends GenericDao<Customer>{
    Customer findCustomerByUser(User user);
    List<Top10CustomerDto> findTop10CustomerTotal();
    List<Order> findOrdersWithAddress(Address address);
}
