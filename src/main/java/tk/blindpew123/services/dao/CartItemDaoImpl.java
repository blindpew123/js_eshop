package tk.blindpew123.services.dao;

import org.springframework.stereotype.Service;
import tk.blindpew123.services.entities.CartItem;

@Service
public class CartItemDaoImpl extends GenericDaoImpl<CartItem> implements CartItemDao {
}
