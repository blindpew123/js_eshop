package tk.blindpew123.services.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

abstract public class GenericDaoImpl<T> implements GenericDao<T> {

    @Autowired
    protected SessionFactory sessionFactory;

    private Class<T> type;

    public GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public T saveOrUpdate(T t) {
        sessionFactory.getCurrentSession().saveOrUpdate(t);
        return t;
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public void delete(T t) {
        sessionFactory.getCurrentSession().remove(t);
    }

    @Transactional(Transactional.TxType.MANDATORY)
    @Override
    public T findById(final long id) {
        return sessionFactory.getCurrentSession().get(getType(), id);
    }

    public Class<T> getType() {
        return type;
    }
}
