package tk.blindpew123.services.dao;

import org.springframework.stereotype.Service;
import tk.blindpew123.services.entities.Address;

@Service
public class AddressDaoImpl extends GenericDaoImpl<Address> implements AddressDao {
}
