package tk.blindpew123.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

@Service
public class FileService {
    private static final Logger logger = LogManager.getLogger(FileService.class.getName());

    public void saveFileService(MultipartFile file, HttpServletRequest request) {
        if (file == null) {
            return;
        }
        try {
             File dest = new File(processingFolders(file.getOriginalFilename(), request));
             file.transferTo(dest);
        } catch (IOException e){
            throw new RuntimeException("File saving error", e);
        }
    }

    private String processingFolders(String fileName, HttpServletRequest request){
        if (fileName != null && !fileName.isEmpty()) {
            String uploadsDir = "/stock/";
            String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
            File uploadRoot = new File(realPathtoUploads);
            if (!uploadRoot.exists()) {
                uploadRoot.mkdir();
            }

            String[] pathElements = fileName.split("/");
            String curFolder = uploadRoot.getAbsolutePath() + "/";
            if (pathElements.length > 1) {
                for (int i = 0; i < pathElements.length - 1; i++) {
                   curFolder = curFolder + pathElements[i] + "/";
                   File part =  new File(curFolder);
                    if (!part.exists()) {
                        part.mkdir();
                    }
                }
            }
            return curFolder + pathElements[pathElements.length - 1];
        }
        return null;
    }

}
