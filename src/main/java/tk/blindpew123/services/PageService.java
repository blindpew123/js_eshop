package tk.blindpew123.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.blindpew123.services.dto.PageElementDescriptor;
import tk.blindpew123.services.entities.Category;

import java.lang.reflect.Array;
import java.security.Principal;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

@Service
public class PageService {
    @Autowired
    private CategoryService categoryService;

    public PageElementDescriptor getCurrentPageDescriptor(final String title,
                                                          final String path,
                                                          final Long menuLevel,
                                                          final Principal principal){
        PageElementDescriptor pageElementDescriptor = new PageElementDescriptor();
        pageElementDescriptor.setContentURI(path);
        pageElementDescriptor.setTitle(title);
        pageElementDescriptor.setCurrentLevelMenu(
                categoryService.getAllCategoriesForParent(menuLevel));
        pageElementDescriptor.setBreadCrumbs(
                createBreadcrumbList(categoryService.getBreadcrumbsForId(menuLevel)));
        pageElementDescriptor.setUserName(principal == null ? null : principal.getName());
        return pageElementDescriptor;
    }

    public List<PageElementDescriptor.Breadcrumb> createBreadcrumbList(final List<Category> categories){
        LinkedList<PageElementDescriptor.Breadcrumb> result = new LinkedList<>();
        for (Category cat : categories){
            PageElementDescriptor.Breadcrumb breadcrumb = new PageElementDescriptor.Breadcrumb();
            breadcrumb.setName(cat.getCategoryName());
            breadcrumb.setUrl("/shop/list/" + cat.getIdCategory());
            result.push(breadcrumb);
        }
        return result;
    }
}
