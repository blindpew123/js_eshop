package tk.blindpew123.services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.blindpew123.services.CategoryService;
import tk.blindpew123.services.dao.ParameterDao;
import tk.blindpew123.services.dao.ParameterTypeDao;
import tk.blindpew123.services.dao.ParameterValueDao;
import tk.blindpew123.services.dto.ParamNameParamValueDto;
import tk.blindpew123.services.dto.ParameterDto;
import tk.blindpew123.services.entities.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
public class ParameterService {

    private static final Logger logger = LogManager.getLogger(ParameterService.class.getName());

    @Autowired
    private ParameterDao parameterDao;

    @Autowired
    private ParameterValueDao parameterValueDao;

    @Autowired
    private ParameterTypeDao parameterTypeDao;

    @Autowired
    private CategoryService categoryService;


    @Transactional
    public Parameter getParameterById(long id){
        return parameterDao.findById(id);
    }

    @Transactional
    public List<ParameterDto> getParametersForCategoryAsDto(final long id){
       Category category = categoryService.getCategoryById(id);
       return parameterDao.findAllParametersForCategoryAsDto(category);
   }

   // All available types for any parameter
    @Transactional
    public List<ParameterType> getAllParameterTypes(){
        return parameterTypeDao.findAllParameterType();
    }

    @Transactional
    public void saveOrUpdateParameterDtoList(final List<ParameterDto> parameterDtoList){
        for (ParameterDto parameterDto: parameterDtoList){
            parameterDao.saveOrUpdate(mapParameterDtoToParameter(parameterDto));
        }

    }

    @Transactional
    protected Parameter mapParameterDtoToParameter(final ParameterDto parameterDto){
       logger.debug(parameterDto.getParameterTypeId());
       logger.debug(parameterDto.getCategoryId());
       Parameter parameter = null;
       if (parameterDto.getIdParameter() == 0) {
           parameter = new Parameter();
       } else {
           parameter = parameterDao.findById(parameterDto.getIdParameter());
           if (parameter == null){
               throw new IllegalArgumentException("Can't find parameter with id: "
               + parameterDto.getParameterTypeId());
           }
       }
       parameter.setParameterName(parameterDto.getParameterName());
       parameter.setCategory(categoryService.getCategoryById(parameterDto.getCategoryId()));
       parameter.setParameterType(parameterTypeDao.findById(parameterDto.getParameterTypeId()));
       parameter.setParameterOrdinal(parameterDto.getParameterOrdinal());
       logger.debug("New Parameter mapped: " + parameter);
       return parameter;
    }

    @Transactional
    public List<String> getAllParameterNamesForCategory(final long id){
        Category category = categoryService.getCategoryById(id);
        return parameterDao.findAllParameterNamesForCategory(category);
    }

    @Transactional
    public List<String> findAllParameterTypesForCategoryAsOrderedList(final long id){
        Category category = categoryService.getCategoryById(id);
        return parameterTypeDao.findAllParameterTypesForCategoryAsOrderedList(category);
    }

    @Transactional
    public List<Parameter> getParametersForCategoryOrdered(final long id){
        Category category  = categoryService.getCategoryById(id);
        return parameterDao.findAllParametersForCategoryOrdered(category);
    }

    @Transactional
    public ParameterValue getParameterValueByShopItemAndParameter(final ShopItem shopItem, final Parameter parameter){
        return parameterDao.findParameterValueByShopItemAndParameter(shopItem, parameter);
    }

    @Transactional
    public ParameterValue saveOrUpdateParameterValue(final ParameterValue parameterValue){
        return parameterValueDao.saveOrUpdate(parameterValue);
    }

    @Transactional
    public List<ParameterValue> getAllValuesForSopItem(final ShopItem shopItem){
        return parameterValueDao.findAllParametersValuesForShopItem(shopItem);
    }

    @Transactional
    public List<ParamNameParamValueDto> getAllParameterNamesAndValues(final ShopItem shopItem){
        return parameterDao.findAllParameterNamesAndValuesOrderedByOrdinalForShopItem(shopItem);
    }

    public List<String> getFullFieldAliasesList(long id){
        List<String> fieldAliasesList = new ArrayList<>(ShopItem.getFieldAliases());
        // add aliases for category's parameters
        fieldAliasesList.addAll(getAllParameterNamesForCategory(id));
        return fieldAliasesList;
    }
    public List<String> getFullFieldTypesList(long id) {
        List<String> fieldTypesList = new ArrayList<>(ShopItem.getFieldTypes());
        fieldTypesList.addAll(findAllParameterTypesForCategoryAsOrderedList(id));
        return fieldTypesList;
    }
    @Transactional
    public void deleteParameter(long id){
        Parameter parameter = parameterDao.findById(id);
        parameterValueDao.deleteAllValuesForParameter(parameter);
        parameterDao.delete(parameter);
    }
}
