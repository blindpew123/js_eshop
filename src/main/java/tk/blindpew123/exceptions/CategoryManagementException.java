package tk.blindpew123.exceptions;

public class CategoryManagementException extends RuntimeException {
    public CategoryManagementException(){ }
    public CategoryManagementException(String message){
        super(message);
    }
    public CategoryManagementException(Exception e){
        super(e);
    }

}
