package tk.blindpew123.exceptions;

import tk.blindpew123.services.entities.User;

public class NoSuchCustomerException extends RuntimeException{
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
