package tk.blindpew123.exceptions;

public class ShopItemManagementException extends RuntimeException {
    public ShopItemManagementException(){ }
    public ShopItemManagementException(String message){
        super(message);
    }
    public ShopItemManagementException(Exception e){
        super(e);
    }
}
