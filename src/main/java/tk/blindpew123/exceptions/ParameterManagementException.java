package tk.blindpew123.exceptions;

public class ParameterManagementException extends RuntimeException{
    public ParameterManagementException(){ }
    public ParameterManagementException(String message){
        super(message);
    }
    public ParameterManagementException(Exception e){
        super(e);
    }
}
