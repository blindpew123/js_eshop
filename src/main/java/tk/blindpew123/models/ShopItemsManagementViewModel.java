package tk.blindpew123.models;

import tk.blindpew123.services.dto.PageElementDescriptor;
import tk.blindpew123.services.entities.Category;
import tk.blindpew123.services.entities.Parameter;
import tk.blindpew123.services.entities.ParameterValue;
import tk.blindpew123.services.entities.ShopItem;

import java.util.List;

/**
 *  Uses to send to view filled List<Category> categoryList object and
 *  List<ParamterType> parameterList object, empty
 *  List<ParameterValue> listParameter object and shopItem ShopItem object.
 *  Also this model include field pageElementDescriptor for properly assembling page template.
 *  From browser controller receives object of the type
 *  with filled fields ShopItem and List<ParameterValues>.
 */

public class ShopItemsManagementViewModel {

    private List<Category> categoryList;
    private List<Parameter> parameterList;
    private ShopItem shopItem;
    private List<ParameterValue> parameterValueList;
    private PageElementDescriptor pageElementDescriptor;

    public List<Category> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    public List<Parameter> getParameterList() {
        return parameterList;
    }

    public void setParameterList(List<Parameter> parameterList) {
        this.parameterList = parameterList;
    }

    public ShopItem getShopItem() {
        return shopItem;
    }

    public void setShopItem(ShopItem shopItem) {
        this.shopItem = shopItem;
    }

    public List<ParameterValue> getParameterValueList() {
        return parameterValueList;
    }

    public void setParameterValueList(List<ParameterValue> parameterValueList) {
        this.parameterValueList = parameterValueList;
    }

    public PageElementDescriptor getPageElementDescriptor() {
        return pageElementDescriptor;
    }

    public void setPageElementDescriptor(PageElementDescriptor pageElementDescriptor) {
        this.pageElementDescriptor = pageElementDescriptor;
    }
}
