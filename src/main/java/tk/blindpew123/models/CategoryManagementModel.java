package tk.blindpew123.models;

import tk.blindpew123.services.entities.Category;

/**
 * Triplet, created for transferring information about position newly created or moved category between
 * different Views and Controllers. Category position descrbies by two other Category elements.
 * One of both is parent and second is before new inserted but at the same level.
 */

public class CategoryManagementModel {
    private Category category = new Category();
    private Category parentCategory = new Category();
    private Category beforeCategory = new Category();

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public Category getBeforeCategory() {
        return beforeCategory;
    }

    public void setBeforeCategory(Category beforeCategory) {
        this.beforeCategory = beforeCategory;
    }
}
