package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import tk.blindpew123.services.CartService;
import tk.blindpew123.services.OrderService;
import tk.blindpew123.services.PageService;
import tk.blindpew123.services.dto.CartListDto;
import tk.blindpew123.services.dto.OrderProcessDto;
import tk.blindpew123.services.entities.Order;
import tk.blindpew123.services.validators.CartValidator;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
@SessionAttributes("cart")
public class OrderController {
    private static final Logger logger = LogManager.getLogger(OrderController.class.getName());

    private static final String PATH_TO_ORDER_FORM = "WEB-INF/jsp/views/processOrder.jsp";
    private static final String PATH_TO_ORDER_FAIL = "WEB-INF/jsp/views/orderFail.jsp";
    private static final String PATH_TO_ORDER_SUCCESS = "WEB-INF/jsp/views/orderSuccess.jsp";
    private static final String INDEX = "/indexpage.jsp";

    @Autowired
    private CartService cartService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private PageService pageService;
    @Autowired
    private CartValidator cartValidator;

    // Отбразить страницу выбора адреса
    // Способа оплаты
    // Способа доставки
    // ------------------------
    // Оформление заказа
    // селект фор апдейт
    // Валидация на наличие - отображение проблем
    // иначе запись в базу

   /* @GetMapping(value = "/cart/processOrder")
    public String orderForm(final @ModelAttribute("cart") CartListDto cartListDto,
                               final @ModelAttribute("cartFormBinding") BindingResult cartBind,
                               final Model model){
       // Здесь мы будем оображать сразу карту с ошибками

    } */
   @ModelAttribute("cart")
   public CartListDto getCartList (HttpServletRequest request) {
       if (request.getSession().getAttribute("cart") == null) {
           logger.debug("New cart");
           return new CartListDto();
       }
       logger.debug("use old");
       return (CartListDto) request.getSession().getAttribute("cart");
   }




   @GetMapping("/cart/orderFail")
   public String getOrderFail(final @ModelAttribute("cart") CartListDto cartListDto,
                              BindingResult bindingResult,
                              final Model model,
                              final Principal principal){
       cartValidator.validate(cartListDto, bindingResult);
       model.addAttribute("pageElementDescriptor",
               pageService.getCurrentPageDescriptor(
                       "Не удалось оформить заказ", PATH_TO_ORDER_FAIL, null, principal));
       return INDEX;
   }

    @GetMapping("/cart/orderSuccess")
    public String getOrderSuccess(final @ModelAttribute("orderId") long orderId,
                                  final Model model,
                                  final Principal principal){
        model.addAttribute("orderId", orderId);
        model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        "Заказ оформлен", PATH_TO_ORDER_SUCCESS, null, principal));
        return INDEX;
    }



   @PostMapping("/cart/processOrder")
   public String makeOrder(final @ModelAttribute("cart") CartListDto cartListDto,

                           final @ModelAttribute("orderDto") OrderProcessDto orderProcessDto,
                           final BindingResult bindingResult,
                           final RedirectAttributes attributes,
                           final Principal principal,
                           final HttpServletRequest request,
                           final SessionStatus sessionStatus){

       // валидация формы в первый раз адрес и т.п.
       // если все в порядке блокируем записи и пишем
       //
       orderProcessDto.setCartListDto((CartListDto) request.getSession().getAttribute("cart"));
       Order order = orderService.processOrder(orderProcessDto, bindingResult, principal);
       logger.info("Order " + (order == null ? "null" : order.getIdOrder()));
       if (order == null){
           // order creation fail

           return "redirect:/cart/orderFail";
       } else {
           ((CartListDto) request.getSession().getAttribute("cart")).getCartItems().clear();
           sessionStatus.setComplete();
           attributes.addFlashAttribute("orderId", order.getIdOrder());
       }
       return "redirect:/cart/orderSuccess";
   }

    @GetMapping(value = "/cart/cartProcess")
    public String getPreparedOrderForm(@ModelAttribute("cart") CartListDto cartListDto,
                                       final BindingResult cartBind,
                                       final Model model,
                                       final Principal principal,
                                       final HttpServletRequest request){

      //artListDto =  (CartListDto) request.getSession().getAttribute("cart");
       model.addAttribute("pageElementDescriptor",
                pageService.getCurrentPageDescriptor(
                        "Оформить заказ", PATH_TO_ORDER_FORM, null, principal));
        // Cart may has problems
        cartService.mergeCart(cartListDto, principal);
        setCartToModel(model, request);
        cartValidator.validate(cartListDto, cartBind);
        request.getSession().setAttribute("cart", cartListDto);
         if (cartBind.hasErrors()){
             // request.getSession().setAttribute("cartFormBinding", cartBind); ??????
                cartService.addResultPreviousCartCheckingToModel(request, model);
                model.addAttribute("cartError", true);
        }
        model.addAttribute("orderDto", orderService.getOrderProcessDto(cartListDto, principal));
        return INDEX;
    }


    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping(value = "/shop/cartProcess", params = "makeOrder")
    public String prepareOrderForm() {
       return "redirect:/cart/cartProcess";
    }

    public void setCartValidator(CartValidator cartValidator) {
        this.cartValidator = cartValidator;
    }

    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    private void setCartToModel(final Model model, final HttpServletRequest request) {
        model.addAttribute("cart", ((CartListDto) request.getSession().getAttribute("cart")));
    }
}
