package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tk.blindpew123.services.OrderService;
import tk.blindpew123.services.dto.OrderAdminDto;
import tk.blindpew123.services.entities.Order;
import tk.blindpew123.services.entities.OrderStatus;
import tk.blindpew123.services.dto.OrderStatusUpdateDto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/admin")
public class OrderManagementController {

    private static final Logger logger = LogManager.getLogger(OrderManagementController.class.getName());

    @Autowired
    private OrderService orderService;

    @GetMapping("orders/{dateStart}/{dateEnd}")
    public List<OrderAdminDto> getOrders(final @PathVariable String dateStart,
                                         final @PathVariable String dateEnd) {
        logger.info(dateStart);
        logger.info(dateEnd);

        LocalDateTime d1 = LocalDateTime.parse(dateStart);
        LocalDateTime d2 = LocalDateTime.parse(dateEnd);
        logger.info(d1);
        logger.info(d2);


        return orderService.getOrderAdminDtoList(d1, d2);
    }

    @GetMapping("/orders/orderStatuses")
    public List<OrderStatus> getOrderStatuses(){
        return orderService.getAllOrderStatuses();
    }

    @PostMapping("/orders/updateOrderStatus")
    public ResponseEntity udaterderStatus(final @RequestBody OrderStatusUpdateDto orderStatusUpdateDto){
        orderService.orderStatusUpdate(orderStatusUpdateDto);
        return new ResponseEntity(HttpStatus.OK);
    }
}
