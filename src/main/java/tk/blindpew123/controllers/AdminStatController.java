package tk.blindpew123.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.blindpew123.services.CustomerService;
import tk.blindpew123.services.OrderService;
import tk.blindpew123.services.ShopItemService;
import tk.blindpew123.services.dto.ShopItemStatTop10Dto;
import tk.blindpew123.services.dto.Top10CustomerDto;

import java.time.LocalDateTime;
import java.util.List;

/**
 *  REST controller provides API for getting information about various statistical parameters
 *  of shop operations
 */
@RestController
@CrossOrigin
@RequestMapping("/admin")
public class AdminStatController {
    @Autowired
    private ShopItemService shopItemService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private OrderService orderService;

    /** Returns List of Top 10 most popular ShopItems in JSON format
     *
     * @return List<ShopItemStatTop10Dto>
     * @see ShopItemStatTop10Dto
     */

    @GetMapping("/stats/top10ItemsQty")
    public List<ShopItemStatTop10Dto> getTop10ItemsQty() {
        return shopItemService.getTop10QtyItems();
    }

    /**
     * Returns List of Top 10 Customers in JSON format
     *
     * @return List<Top10CustomerDto>
     * @see Top10CustomerDto
     */

    @GetMapping("/stats/top10CustomerTotal")
    public List<Top10CustomerDto> getTop10CustomerTotal() {
        return customerService.getTop10CustomerTotal();
    }

    /**
     * Returns total of sales between two dates
     *
     * @param d1 start date
     * @param d2 end date
     * @return total of sales
     */

    @GetMapping("/stats/periodTotal/{d1}/{d2}")
    public Double getTop10CustomerTotal(final @PathVariable String d1,
                                        final @PathVariable String d2) {
        return orderService.getTotalForPeriod(LocalDateTime.parse(d1), LocalDateTime.parse(d2));
    }
}
