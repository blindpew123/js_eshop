package tk.blindpew123.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tk.blindpew123.services.ParameterService;
import tk.blindpew123.services.dto.ParameterDto;
import tk.blindpew123.services.entities.ParameterType;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/admin")
public class ParametersManagementController {

    @Autowired
    private ParameterService parameterService;

    @GetMapping("getParametersForCategory/{id}")
    public List<ParameterDto> getParametersForCategory(final @PathVariable String id){
        return parameterService.getParametersForCategoryAsDto(Long.parseLong(id));
    }

    @GetMapping("getParameterTypes")
    public List<ParameterType> getParameterType(){
        return parameterService.getAllParameterTypes();
    }

    @PostMapping("saveOrUpdateParameterDtoList")
    public ResponseEntity saveOrUpdateParameterDtoList(final @RequestBody List<ParameterDto> parameterDtoList){

        parameterService.saveOrUpdateParameterDtoList(parameterDtoList);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("parameter/{id}/delete")
    public ResponseEntity deleteParameter(final @PathVariable String id){
        parameterService.deleteParameter(Long.parseLong(id));
        return new ResponseEntity(HttpStatus.OK);
    }

}
