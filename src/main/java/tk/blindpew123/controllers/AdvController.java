package tk.blindpew123.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.blindpew123.services.ShopItemService;
import tk.blindpew123.services.dto.ShopItemAdvTop10Dto;
import tk.blindpew123.services.message.MessageSender;

import java.util.ArrayList;
import java.util.List;

/**
 * REST controller provides API for receiving information about Top 10 most popular Shop Items
 */

@RestController
@CrossOrigin
@RequestMapping("/admin")
public class AdvController {

    private static final Logger logger = LogManager.getLogger(AdvController.class.getName());

    @Autowired
    private ShopItemService shopItemService;

    /**
     * Returns List of Top 10 most popular ShopItems
     * @return List<ShopItemAdvTop10Dto>
     * @see ShopItemAdvTop10Dto
     */


    @GetMapping("/shop-items/adv-top10")
    public List<ShopItemAdvTop10Dto> getTopList(){
        logger.debug("Adv Top 10 Items Requested");
        return shopItemService.getAdvTop10QtyItems();
    }
}
