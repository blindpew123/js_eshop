<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:url value="/customer/showUserInformation" var="profile"/>
<div id="wlcm">
    <div class="text-center">
        <h2>Поздравляем с успешной регистрацией, ${userName}</h2>
        <p class="text-sm-center">Пожалуйста укажите о себе <a href="${profile}">дополнительную информацию</a></p>
    </div>
</div>
