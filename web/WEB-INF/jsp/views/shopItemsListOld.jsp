<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div class="container">
    <div class="row">
        <div class="col">
            <span class="text-secondary"><a href="javascript:void(0)" onclick="openNav()"><h5>Настроить отбор</h5></a></span>
        </div>
    </div>
<c:choose>
    <c:when test="${true}">
        <div class="row">
        <c:forEach items="${shopItemsDtoList}" var="item" varStatus="status">
            <c:if test="${status.index > 0 && (status.index % 6) == 0}" >
                </div><div class="row">
            </c:if>

                <div class="card" style="width: 15%; margin-right: 1%">
                    <h6 class="card.header"><a href="<c:url value='/shop/shopItemInfo/${item.idShopItem}' />">${item.itemName}</a></h6>
                    <div class="card-body">
                        <p class="card-text">В наличии: ${item.qnty}</p>
                        <h5>${item.price}</h5><button class="btn btn-primary">В корзину</button>
                    </div>
                </div>


        </c:forEach>
        </div>
    </c:when>
    <c:otherwise>
        <div class="row">
            <div class="col"><h2>По Вашему запросу ничего не найдено!</h2></div>
        </div>
    </c:otherwise>
</c:choose>
</div>