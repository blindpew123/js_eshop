<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="container">
    <div class="row">
        <div class="col">
            <c:url value="/addCategory" var="categoryFormUri"/>
            <form:form  action="${categoryFormUri}"  method="post" modelAttribute="categoryManagementModel" acceptCharset="UTF-8">
                <div class="form-group">
                    <form:label path="category.categoryName">Название категории</form:label>
                    <form:input cssClass="form-control" cssErrorClass="form-control is-invalid" path="category.categoryName" />
                    <div class="invalid-feedback">
                        <form:errors path='category.categoryName' />
                    </div>
                    <form:hidden path="parentCategory.categoryName" />
                    <div class="invalid-feedback">
                        <form:errors path='parentCategory.categoryName' />
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" name="add" value="Добавить" />
            </form:form>
        </div>
    </div>
</div>

