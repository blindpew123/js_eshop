<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="container">
    <div class="row">
        <div class="col">
            <c:url value="/addShopItem" var="shopItemFormUri"/>
            <form:form  action="${shopItemFormUri}"  method="post" modelAttribute="shopItem" acceptCharset="UTF-8">
                <div class="form-group">
                    <form:label path="itemName">Название товара</form:label>
                    <form:input cssClass="form-control" cssErrorClass="form-control is-invalid" path="itemName" />
                    <div class="invalid-feedback">
                        <form:errors path='itemName' />
                    </div>
                    <form:hidden path="category.idCategory" />
                    <div class="invalid-feedback">
                        <form:errors path='category' />
                    </div>
                </div>
                <input type="submit" class="btn btn-primary" name="add" value="Добавить" />
            </form:form>
        </div>
    </div>
</div>
