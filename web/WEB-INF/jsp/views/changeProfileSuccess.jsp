<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="wlcm">
    <div class="text-center">
        <h2>Ваш профиль успешно изменен.</h2>
        <c:if test="${not empty noAddress}">
        <p>Но для оформления заказов Вам нужно будет заполнить хотя бы <a href="${pageContext.request.contextPath}/customer/showUserInformation">один адрес</a>.</p>
        </c:if>
    </div>
</div>
